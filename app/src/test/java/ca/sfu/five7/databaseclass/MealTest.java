package ca.sfu.five7.databaseclass;

import org.junit.Test;

import static org.junit.Assert.*;

public class MealTest {
    final float delta = 10e-15f;

    @Test
    public void hasRated() {
        Meal m = new Meal();
        m.addRating("user1",5);
        m.addRating("user2",0);
        assertEquals(true,m.hasRated("user1"));
        assertEquals(true,m.hasRated("user2"));
        assertEquals(false,m.hasRated("user3"));

    }

    @Test
    public void hasFavourite() {
        Meal m = new Meal();
        m.addFavourite("user1");
        m.addFavourite("user2");
        assertEquals(true,m.hasFavourite("user1"));
        assertEquals(true,m.hasFavourite("user2"));
        assertEquals(false,m.hasFavourite("user3"));
    }


    @Test
    public void getAverage_rating() {
        Meal m = new Meal();
        m.addRating("user1",5);
        m.addRating("user2",0);
        assertEquals(2.5f,m.getAverage_rating(),delta);
    }

    @Test
    public void addRating() {
        Meal m = new Meal();
        m.addRating("user1",5);
        m.addRating("user2",0);
        assertEquals(2.5f,m.getAverage_rating(),delta);
    }

    @Test
    public void removeRating() {
        Meal m = new Meal();
        m.addRating("user1",5);
        m.addRating("user2",0);
        m.removeRating("user2");
        assertEquals(5f,m.getAverage_rating(),delta);
        m.removeRating("user1");
        assertEquals(0f, m.getAverage_rating(),delta);
    }

    @Test
    public void addFavourite() {
        Meal m = new Meal();
        m.addFavourite("user1");
        m.addFavourite("user2");
        assertEquals(2,m.getFavourite_list().size());
        assertEquals("user1",m.getFavourite_list().get(0));
        assertEquals("user2",m.getFavourite_list().get(1));
    }

    @Test
    public void removeFavourite() {
        Meal m = new Meal();
        m.addFavourite("user1");
        m.addFavourite("user2");
        m.removeFavourite("user1");
        assertEquals(1,m.getFavourite_list().size());
        assertEquals("user2",m.getFavourite_list().get(0));


    }
}