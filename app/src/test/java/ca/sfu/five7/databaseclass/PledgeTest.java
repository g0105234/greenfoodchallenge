package ca.sfu.five7.databaseclass;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PledgeTest {

    @Test
    public void toStringTest() {
        Pledge testPledge = new Pledge ("testname", "testamount", "testpic", "testcity", "testid");
        assertEquals (testPledge.toString(), "Pledge{name='testname', pledge_amount='testamount', profile_pic='testpic', city='testcity', user_id='testid'}");
    }
}
