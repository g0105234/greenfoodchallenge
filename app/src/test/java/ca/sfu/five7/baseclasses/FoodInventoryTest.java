package ca.sfu.five7.baseclasses;

import org.junit.Test;

import static org.junit.Assert.*;

/*
    basic test, will improve the test in the future
 */
public class FoodInventoryTest {

    private static final double DELTA = 1e-15;

    @Test
    public void addFood() {
        FoodInventory foodInventory = new FoodInventory();
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        foodInventory.addFood(foodName, co2ePerGram);
        Food food = foodInventory.getFood(0);
        assertEquals(foodName, food.getName());
        assertEquals(co2ePerGram, food.getCo2ePerGram(), DELTA);

    }

    /*
        return according to index
     */
    @Test
    public void getFood() {
        FoodInventory foodInventory = new FoodInventory();
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        foodInventory.addFood(foodName, co2ePerGram);
        Food food = foodInventory.getFood(0);
        assertEquals(foodName, food.getName());
        assertEquals(co2ePerGram, food.getCo2ePerGram(), DELTA);
    }

    /*
        return according to name
     */
    @Test
    public void getFood1() {
        FoodInventory foodInventory = new FoodInventory();
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        foodInventory.addFood(foodName, co2ePerGram);
        Food food = foodInventory.getFood(foodName);
        assertEquals(foodName, food.getName());
        assertEquals(co2ePerGram, food.getCo2ePerGram(), DELTA);
    }

    @Test
    public void removeFood() {
        FoodInventory foodInventory = new FoodInventory();
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        foodInventory.addFood(foodName, co2ePerGram);
        foodInventory.removeFood(foodName);

        assertEquals(null, foodInventory.getFood(foodName));
    }
}