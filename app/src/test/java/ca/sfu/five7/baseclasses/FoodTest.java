package ca.sfu.five7.baseclasses;

import org.junit.Test;

import static org.junit.Assert.*;

/*
    basic test, will improve the test in the future
 */
public class FoodTest {

    private static final double DELTA = 1e-15;

    @Test
    public void getName() {
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        Food food = new Food(foodName,co2ePerGram);
        assertEquals(foodName, food.getName());
    }

    @Test
    public void getCo2ePerGram() {
        String foodName = "apple";
        float co2ePerGram = 0.1f;
        Food food = new Food(foodName,co2ePerGram);
        assertEquals(co2ePerGram, food.getCo2ePerGram(), DELTA);
    }
}