package ca.sfu.five7.baseclasses;

import org.junit.Test;

import static org.junit.Assert.*;

/*
    basic test, will improve the test in the future
 */
public class Co2eCalculatorTest {

    private static final double DELTA = 1e-15;
    @Test
    public void addFood() {
        Co2eCalculator calculator = new Co2eCalculator();
        Food food = new Food("apple", 1);
        calculator.addFood(food, 100);
        float total = calculator.calculateTotal();
        assertEquals(1*100f*52, total, DELTA);
    }

    @Test
    public void setAmount() {
        Co2eCalculator calculator = new Co2eCalculator();
        Food food = new Food("apple", 1);
        calculator.addFood(food, 100);
        calculator.setAmount("apple", 200);
        float total = calculator.calculateTotal();
        assertEquals(1*200f*52, total, DELTA);
    }

    @Test
    public void removeFood() {
        Co2eCalculator calculator = new Co2eCalculator();
        Food food = new Food("apple", 1);
        calculator.addFood(food, 100);
        calculator.setAmount("apple", 200);
        calculator.removeFood("apple");
        float total = calculator.calculateTotal();
        assertEquals(0f, total, DELTA);
    }

    @Test
    public void calculateTotal() {
        Co2eCalculator calculator = new Co2eCalculator();
        Food food = new Food("apple", 1);
        Food food2 = new Food("orange", 2);
        calculator.addFood(food, 100);
        calculator.addFood(food2,200);
        float total = calculator.calculateTotal();
        assertEquals((1*100f+2*200f)*52, total, DELTA);
    }
}