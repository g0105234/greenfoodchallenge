package ca.sfu.five7.databaseclass;

import java.util.ArrayList;
import java.util.List;

public class Meal {
    private String meal_id;
    private String meal_name;
    private String protein_type;
    private String restaurant_name;
    private String description;
    private String city;
    private String location;
    private String meal_pic;
    private List<Ratings> ratings;
    private List<String> favourite_list;

    public Meal(){
        favourite_list = new ArrayList<>();
        ratings = new ArrayList<>();
    }

    public Meal(String meal_id, String meal_name, String protein_type, String restaurant_name, String description, String city, String location
    , String meal_pic){
        this.meal_id = meal_id;
        this.meal_name = meal_name;
        this.protein_type = protein_type;
        this.restaurant_name = restaurant_name;
        this.description = description;
        this.city = city;
        this.location = location;
        this.meal_pic = meal_pic;
        favourite_list = new ArrayList<>();
        ratings = new ArrayList<>();
    }

    public String getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(String meal_id) {
        this.meal_id = meal_id;
    }

    public String getMeal_name() {
        return meal_name;
    }

    public void setMeal_name(String meal_name) {
        this.meal_name = meal_name;
    }

    public String getProtein_type() {
        return protein_type;
    }

    public void setProtein_type(String protein_type) {
        this.protein_type = protein_type;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMeal_pic() {
        return meal_pic;
    }

    public void setMeal_pic(String meal_pic) {
        this.meal_pic = meal_pic;
    }

    public List<Ratings> getRatings(){
        return ratings;
    }

    public float getAverage_rating(){
        if (ratings == null || ratings.size()<=0)
            return 0;
        float total = 0f;

        for (int i = 0; i < ratings.size(); i++){
            total += ratings.get(i).getRating();
        }
        return total/ratings.size();
    }

    public void addRating(String user_id, int rating){
        // check null
        if (ratings == null)
            ratings = new ArrayList<>();
        // avoid repeated ratings
        for (int i = 0; i < ratings.size(); i++){
            if (ratings.get(i).getUser_id().equals(user_id)){
                ratings.get(i).setRating(rating);
                return;
            }
        }
        ratings.add(new Ratings(user_id, rating));
    }

    public void removeRating(String user_id){
        if (ratings == null)
            return;
        for (int i = 0; i < ratings.size(); i++){
            if (ratings.get(i).getUser_id().equals(user_id)){
                ratings.remove(i);
                return;
            }
        }
    }

    public List<String> getFavourite_list() {
        return favourite_list;
    }

    public void addFavourite(String user_id){
        if (favourite_list == null)
            favourite_list = new ArrayList<>();
        // avoid repeated favourite
        for (int i = 0; i < favourite_list.size(); i++){
            if (favourite_list.get(i).equals(user_id)){
                return;
            }
        }
        favourite_list.add(user_id);
    }

    public void removeFavourite(String user_id){
        if (favourite_list == null)
            return;
        for (int i = 0; i < favourite_list.size(); i++){
            if (favourite_list.get(i).equals(user_id)){
                favourite_list.remove(i);
                return;
            }
        }
    }

    public boolean hasRated(String user_id){
        if (ratings == null)
            return false;
        for (int i = 0; i < ratings.size(); i++){
            if (ratings == null)
                continue;
            if (ratings.get(i).getUser_id().equals(user_id)){
                return true;
            }
        }
        return false;
    }

    public boolean hasFavourite(String user_id){
        if (favourite_list == null)
            return false;
        for (int i = 0; i < favourite_list.size(); i++){
            if (favourite_list.get(i).equals(user_id)){
                return true;
            }
        }
        return false;
    }
}
