package ca.sfu.five7.databaseclass;

public class Ratings {
    private String user_id;
    private int rating_score;

    public Ratings(){

    }

    public Ratings(String user_id, int rating_score) {
        this.user_id = user_id;
        this.rating_score = rating_score;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getRating() {
        return rating_score;
    }

    public void setRating(int rating) {
        this.rating_score = rating;
    }
}
