/*
    This class is used to create our database structure on Firebase
*/

package ca.sfu.five7.databaseclass;

public class Pledge {

    private String name;
    private String pledge_amount;
    private String profile_pic;
    private String city;
    private String user_id;

    public Pledge() {
    }

    public Pledge(String name, String pledge_amount, String profile_pic, String city, String user_id) {
        this.name = name;
        this.pledge_amount = pledge_amount;
        this.profile_pic = profile_pic;
        this.city = city;
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPledge_amount() {
        return pledge_amount;
    }

    public void setPledge_amount(String pledge_amount) {
        this.pledge_amount = pledge_amount;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Pledge{" +
                "name='" + name + '\'' +
                ", pledge_amount='" + pledge_amount + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", city='" + city + '\'' +
                ", user_id='" + user_id + '\'' +
                '}';
    }
}
