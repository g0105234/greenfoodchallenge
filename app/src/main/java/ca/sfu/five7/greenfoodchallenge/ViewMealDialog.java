package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ca.sfu.five7.databaseclass.Meal;

public class ViewMealDialog extends DialogFragment {

    ImageView imageView;
    TextView name_text;
    TextView restaurant_text;
    TextView protein_text;
    TextView city_text;
    TextView location_text;
    TextView description_text;
    TextView rating_score_text;
    TextView num_favourite_text;
    ImageButton rate_button_image;
    ImageButton favourite_button_image;
    String meal_id;
    String mealOwner_id;
    boolean hasFavourite;
    boolean hasRate;
    Meal meal;
    float rateValue;

    // key
    public static final String mealOwnerID_key = "meal_owner_id";
    public static final String mealID_key = "meal_id";

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.dialog_view_meal, null);
        builder.setView(view);


        initialize();
        synData();
        setImageSize();
        listenButtons();
        return builder.create();
    }

    @Override
    public void onResume(){
        super.onResume();
        synData();
    }

    private void setImageSize(){
        imageView = view.findViewById(R.id.view_meal_image);
        ViewGroup.LayoutParams params = imageView.getLayoutParams();
        params.height =  Resources.getSystem().getDisplayMetrics().widthPixels;
        params.width =  Resources.getSystem().getDisplayMetrics().widthPixels;
        imageView.setLayoutParams(params);
    }

    public static ViewMealDialog newInstance(String mealOwner_id, String meal_id){
        ViewMealDialog viewMealDialog = new ViewMealDialog();
        Bundle args = new Bundle();

        args.putString(ViewMealDialog.mealOwnerID_key, mealOwner_id);
        args.putString(ViewMealDialog.mealID_key, meal_id);
        viewMealDialog.setArguments(args);
        return  viewMealDialog;
    }

    private void initialize(){
        name_text = view.findViewById(R.id.view_meal_name);
        imageView = view.findViewById(R.id.view_meal_image);
        restaurant_text = view.findViewById(R.id.view_meal_restaurant);
        protein_text = view.findViewById(R.id.view_meal_protein);
        city_text = view.findViewById(R.id.view_meal_city);
        location_text = view.findViewById(R.id.view_meal_location);
        description_text = view.findViewById(R.id.view_meal_description);
        rating_score_text = view.findViewById(R.id.view_meal_rating_score);
        num_favourite_text = view.findViewById(R.id.view_meal_num_favourite);
        rate_button_image = view.findViewById(R.id.view_meal_rate_image_button);
        favourite_button_image = view.findViewById(R.id.view_meal_favourite_image_button);

        //  getting information from bundle
        Bundle args = getArguments();
        meal_id = args.getString(ViewMealDialog.mealID_key);
        mealOwner_id = args.getString(ViewMealDialog.mealOwnerID_key);

    }

    private void setUpLayout(){
        name_text.setText(meal.getMeal_name());
        restaurant_text.setText(meal.getRestaurant_name());
        protein_text.setText(meal.getProtein_type());
        city_text.setText(meal.getCity());
        location_text.setText(meal.getLocation());
        description_text.setText(meal.getDescription());
        if (meal.getRatings().size() == 0)
            rating_score_text.setText(String.valueOf(getString(R.string.view_meal_no_rating)));
        else
            rateValue = meal.getAverage_rating();
            rating_score_text.setText(String.format("%.1f",rateValue));
        num_favourite_text.setText(String.valueOf(meal.getFavourite_list().size()));
        String image = meal.getMeal_pic();
        Glide.with(getContext()).load(image).into(imageView);
    }

    private void listenButtons(){
        listenFavouriteButton();
        listenRateButton();
    }

    private void listenFavouriteButton(){
        favourite_button_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if (!hasFavourite){
                    meal.addFavourite(user_id);
                    favourite_button_image.setImageResource(R.drawable.full_heart);
                }else{
                    meal.removeFavourite(user_id);
                    favourite_button_image.setImageResource(R.drawable.empty_heart);
                }
                FirebaseDatabase.getInstance().getReference()
                        .child(getString(R.string.meal_node))
                        .child(mealOwner_id)
                        .child(meal_id)
                        .setValue(meal);
                synData();
            }
        });
    }

    private void listenRateButton(){
        rate_button_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = RatingDialog.newInstance(mealOwner_id, meal_id, FirebaseAuth.getInstance().getCurrentUser().getUid());
                dialogFragment.show(getFragmentManager(), "rating");
                synData();
            }
        });
    }

    private void synData(){
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.meal_node))
                .child(mealOwner_id).child(meal_id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                meal = dataSnapshot.getValue(Meal.class);
                if (meal == null)
                    return;
                setUpLayout();
                hasFavourite = meal.hasFavourite(user_id);
                hasRate = meal.hasRated(user_id);
                // syn favourite button
                if (hasFavourite){
                    favourite_button_image.setImageResource(R.drawable.full_heart);
                }else{
                    favourite_button_image.setImageResource(R.drawable.empty_heart);
                }

                // syn rate button
                if (hasRate){
                    rate_button_image.setImageResource(R.drawable.full_star);
                }else{
                    rate_button_image.setImageResource(R.drawable.empty_star);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
