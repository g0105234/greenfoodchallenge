package ca.sfu.five7.greenfoodchallenge;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ca.sfu.five7.baseclasses.Co2eCalculator;
import ca.sfu.five7.baseclasses.Food;

public class PlanPageActivity extends AppCompatActivity {

    private static final String EXTRA_RESULT = "ca.sfu.five7.greenfoodchallenge.MealPlan - the result";
    private static final String EXTRA_PLAN = "ca.sfu.five7.greenfoodchallenge.MealPlan - the plan";

    private float userResult;
    private int plan; //the plan chosen 1 = meat, 2 = low meat, 3 = plant based
    private float planResult;
    private int saving;

    private List<Float> servingInGrams;
    private List<Integer> plan1FoodAmounts, plan2FoodAmounts, plan3FoodAmounts;
    private Co2eCalculator calculator;
    private ProgressBar food1Bar, food2Bar, food3Bar, food4Bar, food5Bar, food6Bar, food7Bar;
    DrawerLayout mDrawerLayout;

    private String intentKey = "PledgeAmount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_page);

        extractDataFromIntent();
        setupPlanTitle();
        initializeServingLists();
        setupBarGraph();
        setupTable();
        setupPledgeButton();
        planResult = calculateSavings();
        displaySavings();
        setUpNavBar();
    }

    @Override
    protected  void onResume() {
        super.onResume();
        Button pledgeButton = findViewById(R.id.plan_pledge_button);
        pledgeButton.setEnabled(true);
    }


    private void extractDataFromIntent(){
        Intent intent = getIntent();
        userResult = intent.getFloatExtra(EXTRA_RESULT, 0);
        plan = intent.getIntExtra(EXTRA_PLAN, 1);
    }

    //setup the title of page according to plan chosen
    private void setupPlanTitle(){
        TextView planTitle = findViewById(R.id.plan_title_text_view);
        switch (plan) {
            case 1:
                planTitle.setText(R.string.meat_eater_plan_label);
                break;
            case 2:
                planTitle.setText(R.string.low_meat_plan_label);
                break;
            case 3:
                planTitle.setText(R.string.plant_based_plan_label);
                break;
        }
    }

    private void initializeServingLists() {
        plan1FoodAmounts = new ArrayList<>(Arrays.asList(
                R.string.meat_eater_beef_serving, R.string.meat_eater_pork_serving,
                R.string.meat_eater_chicken_serving, R.string.meat_eater_fish_serving,
                R.string.meat_eater_eggs_serving, R.string.meat_eater_beans_serving,
                R.string.meat_eater_vegetables_serving
        ));

        plan2FoodAmounts = new ArrayList<>(Arrays.asList(
                R.string.low_meat_beef_serving, R.string.low_meat_pork_serving,
                R.string.low_meat_chicken_serving, R.string.low_meat_fish_serving,
                R.string.low_meat_eggs_serving, R.string.low_meat_beans_serving,
                R.string.low_meat_vegetables_serving
        ));

        plan3FoodAmounts = new ArrayList<>(Arrays.asList(
                R.string.plant_beef_serving, R.string.plant_pork_serving,
                R.string.plant_chicken_serving, R.string.plant_fish_serving,
                R.string.plant_eggs_serving, R.string.plant_beans_serving,
                R.string.plant_vegetables_serving
        ));
    }

    private int parse(int id) {
        return Integer.parseInt(getResources().getString(id));
    }

    private void fixTableLabels() {
        checkPlural(R.id.plan_food_1_serving_text_view, R.id.plan_food_1_unit_text_view);
        checkPlural(R.id.plan_food_2_serving_text_view, R.id.plan_food_2_unit_text_view);
        checkPlural(R.id.plan_food_3_serving_text_view, R.id.plan_food_3_unit_text_view);
        checkPlural(R.id.plan_food_4_serving_text_view, R.id.plan_food_4_unit_text_view);
        checkPlural(R.id.plan_food_5_serving_text_view, R.id.plan_food_5_unit_text_view);
        checkPlural(R.id.plan_food_6_serving_text_view, R.id.plan_food_6_unit_text_view);
        checkPlural(R.id.plan_food_7_serving_text_view, R.id.plan_food_7_unit_text_view);
    }

    private void checkPlural(int valueId, int textId) {
        TextView foodServing = findViewById(valueId);
        TextView foodUnit = findViewById(textId);

        if (foodServing.getText().equals("1")) {
            foodUnit.setText(R.string.plan_table_serving_label);
        }else {
            foodUnit.setText(R.string.plan_table_serving_label_plural);
        }
    }

    //setup table values according to plan chosen
    private void setupTable(){
        TextView beefServing = findViewById(R.id.plan_food_1_serving_text_view);
        TextView porkServing = findViewById(R.id.plan_food_2_serving_text_view);
        TextView chickenServing = findViewById(R.id.plan_food_3_serving_text_view);
        TextView fishServing = findViewById(R.id.plan_food_4_serving_text_view);
        TextView eggsServing = findViewById(R.id.plan_food_5_serving_text_view);
        TextView beansServing = findViewById(R.id.plan_food_6_serving_text_view);
        TextView vegetablesServing = findViewById(R.id.plan_food_7_serving_text_view);

        switch (plan) {
            case 1:
                beefServing.setText(plan1FoodAmounts.get(0));
                porkServing.setText(plan1FoodAmounts.get(1));
                chickenServing.setText(plan1FoodAmounts.get(2));
                fishServing.setText(plan1FoodAmounts.get(3));
                eggsServing.setText(plan1FoodAmounts.get(4));
                beansServing.setText(plan1FoodAmounts.get(5));
                vegetablesServing.setText(plan1FoodAmounts.get(6));
                break;
            case 2:
                beefServing.setText(plan2FoodAmounts.get(0));
                porkServing.setText(plan2FoodAmounts.get(1));
                chickenServing.setText(plan2FoodAmounts.get(2));
                fishServing.setText(plan2FoodAmounts.get(3));
                eggsServing.setText(plan2FoodAmounts.get(4));
                beansServing.setText(plan2FoodAmounts.get(5));
                vegetablesServing.setText(plan2FoodAmounts.get(6));
                break;
            case 3:
                beefServing.setText(plan3FoodAmounts.get(0));
                porkServing.setText(plan3FoodAmounts.get(1));
                chickenServing.setText(plan3FoodAmounts.get(2));
                fishServing.setText(plan3FoodAmounts.get(3));
                eggsServing.setText(plan3FoodAmounts.get(4));
                beansServing.setText(plan3FoodAmounts.get(5));
                vegetablesServing.setText(plan3FoodAmounts.get(6));
                break;
        }
        fixTableLabels();
    }

    //setup visual bar graph
    private void setupBarGraph(){
        food1Bar = findViewById(R.id.plan_food_1_progress_bar);
        food2Bar = findViewById(R.id.plan_food_2_progress_bar);
        food3Bar = findViewById(R.id.plan_food_3_progress_bar);
        food4Bar = findViewById(R.id.plan_food_4_progress_bar);
        food5Bar = findViewById(R.id.plan_food_5_progress_bar);
        food6Bar = findViewById(R.id.plan_food_6_progress_bar);
        food7Bar = findViewById(R.id.plan_food_7_progress_bar);

        food1Bar.setMax(7);
        food2Bar.setMax(7);
        food3Bar.setMax(7);
        food4Bar.setMax(7);
        food5Bar.setMax(7);
        food6Bar.setMax(7);
        food7Bar.setMax(7);

        switch(plan) {
            case 1:
                food1Bar.setProgress(parse(plan1FoodAmounts.get(0)));
                food2Bar.setProgress(parse(plan1FoodAmounts.get(1)));
                food3Bar.setProgress(parse(plan1FoodAmounts.get(2)));
                food4Bar.setProgress(parse(plan1FoodAmounts.get(3)));
                food5Bar.setProgress(parse(plan1FoodAmounts.get(4)));
                food6Bar.setProgress(parse(plan1FoodAmounts.get(5)));
                food7Bar.setProgress(parse(plan1FoodAmounts.get(6)));
                break;
            case 2:
                food1Bar.setProgress(parse(plan2FoodAmounts.get(0)));
                food2Bar.setProgress(parse(plan2FoodAmounts.get(1)));
                food3Bar.setProgress(parse(plan2FoodAmounts.get(2)));
                food4Bar.setProgress(parse(plan2FoodAmounts.get(3)));
                food5Bar.setProgress(parse(plan2FoodAmounts.get(4)));
                food6Bar.setProgress(parse(plan2FoodAmounts.get(5)));
                food7Bar.setProgress(parse(plan2FoodAmounts.get(6)));
                break;
            case 3:
                food1Bar.setProgress(parse(plan3FoodAmounts.get(0)));
                food2Bar.setProgress(parse(plan3FoodAmounts.get(1)));
                food3Bar.setProgress(parse(plan3FoodAmounts.get(2)));
                food4Bar.setProgress(parse(plan3FoodAmounts.get(3)));
                food5Bar.setProgress(parse(plan3FoodAmounts.get(4)));
                food6Bar.setProgress(parse(plan3FoodAmounts.get(5)));
                food7Bar.setProgress(parse(plan3FoodAmounts.get(6)));
                break;
        }
    }

    private void setupPledgeButton(){
        final Button pledgeButton = findViewById(R.id.plan_pledge_button);
        pledgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pledgeButton.setEnabled(false);

                //start Pledge activity
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                        .child(getString(R.string.user_node))
                        .child(user.getUid());

                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (saving < 0)
                            saving = 0;
                        if(!dataSnapshot.exists()) {
                            Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                            intent.putExtra(intentKey,saving);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                            intent.putExtra(intentKey,saving);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                mDrawerLayout.closeDrawers();
            }
        });
    }

    private float calculateSavings(){
        servingInGrams = new ArrayList<>(Arrays.asList(0.075f,0.075f,0.075f,0.075f,0.090f,0.160f,0.100f));
        initializeCalculator();
        return calculator.calculateTotal();
    }

    //setup the calculator to have all needed values from whichever plan chosen
    private void initializeCalculator(){
        calculator = new Co2eCalculator();
        int index = 0;
        switch(plan) {
            case 1:
                addFoodToCalculator(R.string.food_1_name, R.string.food_1_co2e, R.string.meat_eater_beef_serving,index);
                index += 1;
                addFoodToCalculator(R.string.food_2_name, R.string.food_2_co2e, R.string.meat_eater_pork_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_3_name, R.string.food_3_co2e, R.string.meat_eater_chicken_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_4_name, R.string.food_4_co2e, R.string.meat_eater_fish_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_5_name, R.string.food_5_co2e, R.string.meat_eater_eggs_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_6_name, R.string.food_6_co2e, R.string.meat_eater_beans_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_7_name, R.string.food_7_co2e, R.string.meat_eater_vegetables_serving, index);
                break;
            case 2:
                addFoodToCalculator(R.string.food_1_name, R.string.food_1_co2e, R.string.low_meat_beef_serving,index);
                index += 1;
                addFoodToCalculator(R.string.food_2_name, R.string.food_2_co2e, R.string.low_meat_pork_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_3_name, R.string.food_3_co2e, R.string.low_meat_chicken_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_4_name, R.string.food_4_co2e, R.string.low_meat_fish_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_5_name, R.string.food_5_co2e, R.string.low_meat_eggs_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_6_name, R.string.food_6_co2e, R.string.low_meat_beans_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_7_name, R.string.food_7_co2e, R.string.low_meat_vegetables_serving, index);
                break;
            case 3:
                addFoodToCalculator(R.string.food_1_name, R.string.food_1_co2e, R.string.plant_beef_serving,index);
                index += 1;
                addFoodToCalculator(R.string.food_2_name, R.string.food_2_co2e, R.string.plant_pork_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_3_name, R.string.food_3_co2e, R.string.plant_chicken_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_4_name, R.string.food_4_co2e, R.string.plant_fish_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_5_name, R.string.food_5_co2e, R.string.plant_eggs_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_6_name, R.string.food_6_co2e, R.string.plant_beans_serving, index);
                index += 1;
                addFoodToCalculator(R.string.food_7_name, R.string.food_7_co2e, R.string.plant_vegetables_serving, index);
                break;
        }
    }

    //get all the numbers needed to calculate
    private void addFoodToCalculator(int nameId, int co2eId,int foodServing, int index){
        String foodName = getResources().getString(nameId);
        Float co2ePerGram = Float.parseFloat(getResources().getString(co2eId));
        Float foodServingSize = Float.parseFloat(getResources().getString(foodServing));
        Float tmpServing = servingInGrams.get(index) * foodServingSize;
        servingInGrams.set(index, tmpServing);

        Food food = new Food(foodName, co2ePerGram);
        calculator.addFood(food, servingInGrams.get(index));
    }

    private void displaySavings(){
        saving = (int) userResult - (int) planResult;
        TextView savingMessageTextView = findViewById(R.id.plan_savings_message_text_view);
        TextView savingTextView = findViewById(R.id.plan_savings_text_view);
        String message;
        if (saving > 0) {
            message = " " + saving + " " + getResources().getString(R.string.plan_units_kg_co2e) + " of CO2e!";
            savingMessageTextView.setVisibility(TextView.VISIBLE);
            savingTextView.setVisibility(TextView.VISIBLE);
            savingTextView.setText(message);
        } else {
            message = getResources().getString(R.string.ineffective_plan_message);
            savingMessageTextView.setVisibility(TextView.GONE);
            savingTextView.setText(message);
        }
        displayVancouverSavings(saving);
        displayEquivalences(saving);
    }

    private void displayVancouverSavings(int saving){
        TextView vancouverSavingMessageTextView = findViewById(R.id.plan_population_savings_message_text_view);
        TextView vancouverSavingTextView = findViewById(R.id.plan_population_savings_text_view);

        if(saving > 0) {
            int vancouverSavings = (int) (2463000 * 0.90 * saving);
            vancouverSavings = vancouverSavings / 1000;
            String message = " " + vancouverSavings + " " + getResources().getString(R.string.plan_units_tonnes_co2e) + " of CO2e!!";
            vancouverSavingMessageTextView.setVisibility(TextView.VISIBLE);
            vancouverSavingTextView.setVisibility(TextView.VISIBLE);
            vancouverSavingTextView.setText(message);
        } else {
            vancouverSavingMessageTextView.setVisibility(TextView.GONE);
            vancouverSavingTextView.setVisibility(TextView.GONE);
        }
    }

    private void displayEquivalences(int saving){
        TextView equivalenceTextView = findViewById(R.id.plan_equivalences);
        TextView drivingEquivalenceMessage = findViewById(R.id.plan_equivalences_driving_message);
        TextView drivingEquivalence = findViewById(R.id.plan_equivalences_driving);
        TextView lightEquivalenceMessage = findViewById(R.id.plan_equivalences_lights_message);
        TextView lightEquivalence = findViewById(R.id.plan_equivalences_lights);
        TextView treesEquivalenceMessage = findViewById(R.id.plan_equivalences_trees_message);
        TextView treesEquivalence = findViewById(R.id.plan_equivalences_trees);

        if(saving > 0) {
            String equivalenceMessage = "Saving " + saving + " " + getResources().getString(R.string.plan_units_kg_co2e) + " is equivalent to:";
            equivalenceTextView.setVisibility(TextView.VISIBLE);
            equivalenceTextView.setText(equivalenceMessage);

            //get driving equivalence
            int drivingSaving = saving * 4;
            String drivingMessage = " " + drivingSaving + " " + getResources().getString(R.string.driving_equivalence_2);
            drivingEquivalenceMessage.setVisibility(TextView.VISIBLE);
            drivingEquivalence.setVisibility(TextView.VISIBLE);
            drivingEquivalence.setText(drivingMessage);

            //get switching to LED's
            int lightSaving = (int) (saving * 0.333);
            String lightMessage = " " + lightSaving + " " + getResources().getString(R.string.light_equivalence_2);
            lightEquivalenceMessage.setVisibility(TextView.VISIBLE);
            lightEquivalence.setVisibility(TextView.VISIBLE);
            lightEquivalence.setText(lightMessage);

            //get planting trees
            int treesSaving = (int) (saving / 21.77);
            String treesMessage = " " + treesSaving + " " + getResources().getString(R.string.trees_equivalence_2);
            treesEquivalenceMessage.setVisibility(TextView.VISIBLE);
            treesEquivalence.setVisibility(TextView.VISIBLE);
            treesEquivalence.setText(treesMessage);
        } else {
            equivalenceTextView.setVisibility(TextView.GONE);
            drivingEquivalenceMessage.setVisibility(TextView.GONE);
            drivingEquivalence.setVisibility(TextView.GONE);
            lightEquivalenceMessage.setVisibility(TextView.GONE);
            lightEquivalence.setVisibility(TextView.GONE);
            treesEquivalenceMessage.setVisibility(TextView.GONE);
            treesEquivalence.setVisibility(TextView.GONE);
        }
    }


    public static Intent makeIntent(Context context, float result, int plan){
        Intent intent = new Intent(context, PlanPageActivity.class);
        intent.putExtra(EXTRA_RESULT, result);
        intent.putExtra(EXTRA_PLAN, plan);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar() {
        mDrawerLayout = findViewById(R.id.drawer_layout_plans);

        Toolbar toolbar = findViewById(R.id.plan_toolbar);
        toolbar.setTitle(getString(R.string.plans_page));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.plan_navigation_view);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
