package ca.sfu.five7.greenfoodchallenge;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupMenu;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ca.sfu.five7.databaseclass.Meal;
import ca.sfu.five7.adapters.MealViewAdapter;

public class ViewMealActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    FloatingActionButton filterButton;
    List<Meal> mMealList;
    List<String> ownerList;

    String proteinChosen;
    String favouriteChosen;
    String cityChosen;
    String imageChosen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_view);
        mMealList = new ArrayList<>();
        ownerList = new ArrayList<>();
        mRecyclerView = findViewById(R.id.meal_view_recycler_view);
        filterButton = findViewById(R.id.view_meal_filter_button);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setUpNavBar();
        // initialize filter strings
        resetFilter();
        getDataFromDatabase();
        setupFilterButton();
    }

    @Override
    protected void onResume(){
        super.onResume();
        resetFilter();
        getDataFromDatabase();
    }

    private void resetFilter(){
        cityChosen = getString(R.string.filter_all_cities);
        proteinChosen = getString(R.string.filter_all_protein);
        favouriteChosen = getString(R.string.filter_all_favourite);
        imageChosen = getString(R.string.filter_all_image);
    }



    private void getDataFromDatabase() {
        final String mCityChosen = cityChosen;
        final String mProteinChosen = proteinChosen;
        final String mFavouriteChosen = favouriteChosen;
        final String imageChosen = this.imageChosen;

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.meal_node));
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mMealList.clear();
                ownerList.clear();
                boolean correctCity;
                boolean correctProtein;
                boolean correctFavourite;
                boolean correctImage;
                boolean exist;
                String mUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                List<String> favourite_list;
                for (DataSnapshot userSnapShot : dataSnapshot.getChildren())
                    for(DataSnapshot mealSnapshot : userSnapShot.getChildren()){
                        Meal meal = mealSnapshot.getValue(Meal.class);
                        // city filter
                        if (mCityChosen.equals(getString(R.string.filter_all_cities))){
                            correctCity = true;
                        } else if(meal.getCity().equals(mCityChosen)){
                            correctCity = true;
                        }else{
                            correctCity = false;
                        }
                        // protein filter
                        if (mProteinChosen.equals(getString(R.string.filter_all_protein))) {
                            correctProtein = true;
                        }else if (meal.getProtein_type().equals(mProteinChosen)){
                            correctProtein = true;
                        }else {
                            correctProtein = false;
                        }
                        // favourite filter
                        if (mFavouriteChosen.equals(getString(R.string.filter_all_favourite))){
                            correctFavourite = true;
                        } else{
                            favourite_list = meal.getFavourite_list();
                            exist = false;
                            for(String userID : favourite_list){
                                if (userID.equals(mUserID)){
                                    exist = true;
                                }
                            }
                            if (mFavouriteChosen.equals(getString(R.string.filter_favourite))){
                                correctFavourite = exist;
                            }else{
                                correctFavourite = !exist;
                            }
                        }
                        // image filter
                        if (imageChosen.equals(getString(R.string.filter_all_image))){
                            correctImage = true;
                        }else if (imageChosen.equals(getString(R.string.filter_with_image))){
                            correctImage = false;
                            if (!meal.getMeal_pic().equals(getString(R.string.default_meal_image))){
                                correctImage = true;
                            }
                        }else{
                            correctImage = false;
                            if (meal.getMeal_pic().equals(getString(R.string.default_meal_image))){
                                correctImage = true;
                            }
                        }

                        if (correctCity && correctProtein && correctFavourite && correctImage) {
                            ownerList.add(userSnapShot.getKey());
                            mMealList.add(meal);
                        }
                }

                mAdapter = new MealViewAdapter(ownerList, mMealList, getApplicationContext(), getSupportFragmentManager());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setupFilterButton(){
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(getApplicationContext(), filterButton);
                popupMenu.getMenuInflater().inflate(R.menu.view_meal_page_filter_options, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String itemText = item.toString();
                        if(itemText.equals(getString(R.string.filter_by_city_title))){
                            createFilterDialog(R.string.city_filter_dialog_title, R.array.metro_vancouver_cities_filter);
                        }else if(itemText.equals(getString(R.string.filter_by_protein_title))){
                            createFilterDialog(R.string.protein_filter_dialog_title, R.array.protein_types_filter);
                        }else if(itemText.equals(getString(R.string.filter_by_favourite_title))){
                            createFilterDialog(R.string.favourite_filter_dialog_title, R.array.favourite_types_filter);
                        }else if (itemText.equals(getString(R.string.filter_by_image_title))){
                            createFilterDialog(R.string.image_filter_dialog_title, R.array.image_types_filter);
                        }else if (itemText.equals(getString(R.string.view_meal_reset_filter_string))){
                            resetFilter();
                            getDataFromDatabase();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void createFilterDialog(final int dialogTitlteID, final int typesFilterID){
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(dialogTitlteID)
                .setItems(typesFilterID, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch(dialogTitlteID) {
                            case R.string.city_filter_dialog_title:
                                cityChosen = getResources().getStringArray(typesFilterID)[which];
                                break;
                            case R.string.protein_filter_dialog_title:
                                proteinChosen = getResources().getStringArray(typesFilterID)[which];
                                break;
                            case R.string.favourite_filter_dialog_title:
                                favouriteChosen = getResources().getStringArray(typesFilterID)[which];
                                break;
                            case R.string.image_filter_dialog_title:
                                imageChosen = getResources().getStringArray(typesFilterID)[which];
                                break;
                        }
                        
                        getDataFromDatabase();
                    }
                });

        // creating alert dialog
        android.app.AlertDialog dialog = builder.create();


        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar(){
        //change
        mDrawerLayout = findViewById(R.id.drawer_layout_meal_view_activity);
        Toolbar toolbar = findViewById(R.id.toolbar_meal_view);
        toolbar.setTitle(R.string.view_meal_page);
        final NavigationView nav_view = findViewById(R.id.navigation_view_meals);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        //update meals page
                        resetFilter();
                        getDataFromDatabase();
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
