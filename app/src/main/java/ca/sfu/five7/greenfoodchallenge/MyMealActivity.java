package ca.sfu.five7.greenfoodchallenge;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ca.sfu.five7.databaseclass.Meal;
import ca.sfu.five7.adapters.MyMealAdapter;

public class MyMealActivity extends AppCompatActivity {

    FloatingActionButton addMealButton;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    DrawerLayout mDrawerLayout;
    TextView noMealTextView;

    List<Meal> mMealList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meal);

        noMealTextView = findViewById(R.id.no_meal_text_view);
        addMealButton = findViewById(R.id.add_meal_button);
        mRecyclerView = findViewById(R.id.meal_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mMealList = new ArrayList<>();

        addMealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new AddMealDialog();
                dialogFragment.show(getSupportFragmentManager(), "meal");
            }
        });

        setUpNavBar();
        getDataFromDatabase();
    }

    private void getDataFromDatabase() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                    .child(getString(R.string.meal_node))
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mMealList.clear();
                for(DataSnapshot mealSnapshot : dataSnapshot.getChildren()){
                    Meal meal = mealSnapshot.getValue(Meal.class);

                    mMealList.add(meal);
                }

                if(mMealList.isEmpty()){
                    noMealTextView.setVisibility(View.VISIBLE);
                }else{
                    noMealTextView.setVisibility(View.INVISIBLE);
                }

                mAdapter = new MyMealAdapter(mMealList, MyMealActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar(){
        //change
        mDrawerLayout = findViewById(R.id.drawer_layout_meal);
        Toolbar toolbar = findViewById(R.id.toolbar_meal);
        toolbar.setTitle(R.string.my_meal_page);
        final NavigationView nav_view = findViewById(R.id.navigation_my_meal);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
