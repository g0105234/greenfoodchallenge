package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import ca.sfu.five7.databaseclass.Meal;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class AddMealDialog extends DialogFragment {

    public static final int IMAGE_REQUEST = 101;

    EditText nameEditText;
    EditText restaurantEditText;
    EditText locationEditText;
    EditText descriptionEditText;
    Spinner citySpinner;
    Spinner proteinSpinner;
    ImageView mealImage;
    Button saveMealButton;
    Button cancelButton;
    ArrayAdapter<CharSequence> cityAdapter;
    ArrayAdapter<CharSequence> proteinAdapter;

    String cityText;
    String proteinText;

    private StorageReference mStorageReference;

    private Uri filePath;
    private String firebaseUrl;
    private String mealID;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_add_meal, null);
        builder.setView(view);

        nameEditText = view.findViewById(R.id.add_meal_name);
        restaurantEditText = view.findViewById(R.id.add_meal_restaurant);
        proteinSpinner = view.findViewById(R.id.add_meal_protein_spinner);
        citySpinner = view.findViewById(R.id.add_meal_city_spinner);
        locationEditText = view.findViewById(R.id.add_meal_location);
        descriptionEditText = view.findViewById(R.id.add_meal_description);
        saveMealButton = view.findViewById(R.id.add_meal_save_button);
        cancelButton = view.findViewById(R.id.add_meal_cancel_button);
        mealImage = view.findViewById(R.id.add_meal_image);
        mealImage.setImageResource(R.drawable.carrot_logo);

        mealID = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.meal_node))
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .push()
                .getKey();


        // set default image
        firebaseUrl = getString(R.string.default_meal_image);
        changeMealImage();
        setupCitySpinnerSelection();
        setupProteinSpinnerSelection();
        setupCancelButton();
        setupSaveMealButton();

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction()!=KeyEvent.ACTION_UP) {

                    if(!firebaseUrl.equals(getString(R.string.default_meal_image))){
                        FirebaseStorage.getInstance().getReferenceFromUrl(firebaseUrl).delete();
                    }

                    getDialog().dismiss();

                    return true;
                }
                return false;
            }
        });
    }

    private void setupProteinSpinnerSelection(){
        // setup spinner
        proteinAdapter = ArrayAdapter.createFromResource(getContext(), R.array.protein_types, android.R.layout.simple_spinner_item);
        proteinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        proteinSpinner.setAdapter(proteinAdapter);
        proteinSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proteinText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupCitySpinnerSelection(){
        // setup spinner
        cityAdapter = ArrayAdapter.createFromResource(getContext(), R.array.metro_vancouver_cities, android.R.layout.simple_spinner_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupCancelButton() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!firebaseUrl.equals(getString(R.string.default_meal_image))){
                    FirebaseStorage.getInstance().getReferenceFromUrl(firebaseUrl).delete();
                }

                getDialog().dismiss();
            }
        });
    }

    private void setupSaveMealButton(){
        saveMealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean dataFilledIn = checkEditText();
                if(dataFilledIn == true) {
                    saveToDatabase();
                }
            }
        });
    }

    private boolean checkEditText() {

        if(TextUtils.isEmpty(nameEditText.getText().toString())){
            nameEditText.setError(getString(R.string.add_meal_name_error));
            return false;
        }
        if(TextUtils.isEmpty(restaurantEditText.getText().toString())){
            restaurantEditText.setError(getString(R.string.add_meal_restaurant_error));
            return false;
        }
        if(TextUtils.isEmpty(locationEditText.getText().toString())){
            locationEditText.setError(getString(R.string.add_meal_location_error));
            return false;
        }
        return true;
    }

    private void saveToDatabase() {
        String name = nameEditText.getText().toString();
        String restaurant = restaurantEditText.getText().toString();
        String location = locationEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        String mealUrl = firebaseUrl;

        Meal userMeal = new Meal(mealID,name,proteinText,restaurant,description,cityText,location,mealUrl);

        FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.meal_node))
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(mealID)
                .setValue(userMeal).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Meal Added", Toast.LENGTH_SHORT).show();
            }
        });
        getDialog().dismiss();
    }

    private void changeMealImage() {
        mealImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                Glide.with(getApplicationContext()).load(bitmap).into(mealImage);
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {

        if(filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            mStorageReference = FirebaseStorage.getInstance().getReference();

            final StorageReference ref = mStorageReference.child("images/users/"+ FirebaseAuth.getInstance().getCurrentUser().getUid() + "/meals/" + mealID);
            //add file on Firebase and get Download Link
            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        progressDialog.dismiss();
                        firebaseUrl = task.getResult().toString();
                    }
                }
            });
        }
    }

}
