package ca.sfu.five7.greenfoodchallenge;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import ca.sfu.five7.databaseclass.Pledge;

public class AddPledgePageActivity extends AppCompatActivity {

    public static final int IMAGE_REQUEST = 101;
    private Button saveButton;
    private Pledge userPledge;
    private EditText nameText;
    private Spinner citySpinner;
    private EditText pledgeAmountText;
    private ImageView profilePic;
    private DrawerLayout mDrawerLayout;
    private StorageReference mStorageReference;
    private String cityText;
    private ArrayAdapter<CharSequence> cityAdapter;

    private Uri filePath;
    private String firebaseUrl;

    private String intentKey = "PledgeAmount";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pledge_page);

        nameText = findViewById(R.id.add_pledge_name_edit_text);
        citySpinner = findViewById(R.id.add_pledge_city_edit_text);
        pledgeAmountText = findViewById(R.id.add_pledge_amount_edit_text);
        profilePic = findViewById(R.id.add_pledge_user_photo);
        profilePic.setImageResource(R.drawable.default_user_pfp);

        //Set up the the spinner
        cityAdapter = ArrayAdapter.createFromResource(this, R.array.metro_vancouver_cities, android.R.layout.simple_spinner_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);
        cityText = citySpinner.getItemAtPosition(0).toString();

        setUpPledgeAmountText();

        setUpNavBar();
        setupCitySpinnerSelection();
        //set default image
        firebaseUrl = getString(R.string.default_profile_image);
        changeProfileImage();
        saveData();
    }

    private void setUpPledgeAmountText(){
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        // Check if there is value
        if (intent.hasExtra(intentKey)){
            pledgeAmountText.setText(String.valueOf(extras.getInt(intentKey)));
        }
    }

    private void setupCitySpinnerSelection(){
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void changeProfileImage() {
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(getApplicationContext()).load(bitmap).into(profilePic);
                mStorageReference = FirebaseStorage.getInstance().getReference();
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void saveData() {
        saveButton = findViewById(R.id.add_pledge_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean dataFilledIn = checkEditText();
                if(dataFilledIn) {
                    setupData();
                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private boolean checkEditText() {
        if(TextUtils.isEmpty(nameText.getText().toString())){
            nameText.setError("Must fill in Name");
            return false;
        }
        if(TextUtils.isEmpty(pledgeAmountText.getText().toString())){
            pledgeAmountText.setError("Must fill in an Amount");
            return false;
        }
        if(Integer.parseInt(pledgeAmountText.getText().toString()) > 2000){
            pledgeAmountText.setError("Maximum you can pledge is 2000 kg of CO2e");
            return false;
        }

        return true;
    }

    private void uploadImage() {

        if(filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = mStorageReference.child("images/users/"+ FirebaseAuth.getInstance().getCurrentUser().getUid() + "/profile_pic");
            //add file on Firebase and get Download Link
            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        progressDialog.dismiss();
                        firebaseUrl = task.getResult().toString();
                    }
                }
            });
        }
    }

    private void setupData() {

        String name = nameText.getText().toString();
        String city = cityText;
        String pledgeAmount = pledgeAmountText.getText().toString();
        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String profile_pic = firebaseUrl;


        userPledge = new Pledge(name, pledgeAmount, profile_pic, city, userID);

        FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.user_node))
                .child(userID)
                .setValue(userPledge).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Pledge Added", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar(){
        mDrawerLayout = findViewById(R.id.drawer_layout_add_pledge);

        Toolbar toolbar = findViewById(R.id.toolbar_add_pledge);
        toolbar.setTitle(R.string.add_pledge_page);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.navigation_view_add_pledge);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case R.id.nav_home:

                        intent = new Intent (getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_pledge:
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                }
                return true;
            }
        });
    }
}
