package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

import ca.sfu.five7.databaseclass.Pledge;

public class EditPledgePageActivity extends AppCompatActivity {

    public static final int IMAGE_REQUEST = 101;
    private Button saveButton;
    private ShareButton shareButton;
    private Pledge userPledge;
    private EditText nameEditText;
    private Spinner citySpinner;
    private EditText pledgeAmountEditText;
    private ImageView profilePic;
    private DrawerLayout mDrawerLayout;
    private StorageReference mStorageReference;
    private String cityText;
    private ArrayAdapter<CharSequence> cityAdapter;
    private boolean inSharing = false;
    private TextView nameTextView;
    private TextView citySpinnerTextView;
    private TextView pledgeAmountTextView;
    private TextView cityTextView;

    private Uri filePath;
    private String firebaseUrl;

    private int pledgeAmount;
    private String intentKey = "PledgeAmount";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pledge_page);

        shareButton = findViewById(R.id.share_button);
        nameEditText = findViewById(R.id.edit_pledge_name_edit_text);
        citySpinner = findViewById(R.id.edit_pledge_city_edit_text);
        pledgeAmountEditText = findViewById(R.id.edit_pledge_amount_edit_text);
        saveButton = findViewById(R.id.edit_pledge_save_button);
        profilePic = findViewById(R.id.edit_pledge_user_photo);
        nameTextView = findViewById(R.id.edit_pledge_name_text_view);
        citySpinnerTextView = findViewById(R.id.edit_pledge_city_spinner_text_view);
        cityTextView = findViewById(R.id.edit_pledge_city_text_view);
        pledgeAmountTextView = findViewById(R.id.edit_pledge_amount_text_view);

        //Set up the the spinner
        cityAdapter = ArrayAdapter.createFromResource(this, R.array.metro_vancouver_cities, android.R.layout.simple_spinner_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);

        updateInformation();
        hideEditText();

        setUpPledgeAmountText();
        setUpNavBar();
        setupCitySpinnerSelection();
        changeProfileImage();
        saveData();



        setUpShareButton();
    }

    private void setUpPledgeAmountText(){
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        // Check if there is value
        if (intent.hasExtra(intentKey)){
            showEditText();
            pledgeAmount = extras.getInt(intentKey);
            pledgeAmountEditText.setText(String.valueOf(pledgeAmount));
            String pledgeAmountText = String.valueOf(pledgeAmount) + " " + getResources().getString(R.string.plan_units_kg_co2e) + " " + getResources().getString(R.string.co2e);
            pledgeAmountTextView.setText(pledgeAmountText);
        }
    }



    public void confirmDelete(String message) {
        // generating AlertDialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HelpPopUpTheme);
        builder.setMessage(message);

        // set up buttons
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                        .child(getString(R.string.user_node))
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

                ref.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                if (!firebaseUrl.equals(getString(R.string.default_profile_image)))
                    FirebaseStorage.getInstance().getReferenceFromUrl(firebaseUrl).delete();
            }
        });

        builder.setNegativeButton("Cancel", null);

        // creating alert dialog
        AlertDialog dialog = builder.create();


        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);


    }

    private void setupCitySpinnerSelection(){
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityText = parent.getItemAtPosition(position).toString();
                citySpinnerTextView.setText(parent.getItemAtPosition(position).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showEditText() {
        nameEditText.setVisibility(View.VISIBLE);
        citySpinner.setVisibility(View.VISIBLE);
        pledgeAmountEditText.setVisibility(View.VISIBLE);
        nameTextView.setVisibility(View.INVISIBLE);
        citySpinnerTextView.setVisibility(View.INVISIBLE);
        pledgeAmountTextView.setVisibility(View.INVISIBLE);
        profilePic.setEnabled(true);
        shareButton.setVisibility(View.GONE);
        saveButton.setVisibility(View.VISIBLE);
        cityTextView.setVisibility(View.VISIBLE);
    }

    private void hideEditText() {
        nameEditText.setVisibility(View.INVISIBLE);
        citySpinner.setVisibility(View.INVISIBLE);
        pledgeAmountEditText.setVisibility(View.INVISIBLE);
        nameTextView.setVisibility(View.VISIBLE);
        citySpinnerTextView.setVisibility(View.VISIBLE);
        pledgeAmountTextView.setVisibility(View.VISIBLE);
        profilePic.setEnabled(false);
        shareButton.setVisibility(View.VISIBLE);
        saveButton.setVisibility(View.GONE);
        cityTextView.setVisibility(View.INVISIBLE);
    }

    private void updateInformation() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Downloading...");
        progressDialog.show();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.user_node))
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userPledge = dataSnapshot.getValue(Pledge.class);

                nameEditText.setText(userPledge.getName());
                nameTextView.setText(userPledge.getName());
                //Set up the citySpinner to have right value
                cityText = userPledge.getCity();
                citySpinnerTextView.setText(userPledge.getCity());
                int spinnerSelection = cityAdapter.getPosition(cityText);
                citySpinner.setSelection(spinnerSelection);

                // if activity start not from plan page
                Intent intent = getIntent();
                if (!intent.hasExtra(intentKey)){
                    pledgeAmountEditText.setText(userPledge.getPledge_amount());
                    String pledgeAmountText = String.valueOf(userPledge.getPledge_amount()) + " " + getResources().getString(R.string.plan_units_kg_co2e) + " " + getResources().getString(R.string.co2e);
                    pledgeAmountTextView.setText(pledgeAmountText);
                    pledgeAmount = Integer.parseInt(userPledge.getPledge_amount());
                }

                Glide.with(getApplicationContext()).load(userPledge.getProfile_pic()).into(profilePic);
                firebaseUrl = userPledge.getProfile_pic();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        progressDialog.dismiss();
    }

    private void changeProfileImage() {
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    profilePic.setClickable(false);
                    chooseImage();
                    profilePic.setClickable(true);
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Glide.with(getApplicationContext()).load(bitmap).into(profilePic);
                mStorageReference = FirebaseStorage.getInstance().getReference();
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {

        if(filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = mStorageReference.child("images/users/"+ FirebaseAuth.getInstance().getCurrentUser().getUid() + "/profile_pic");
            //add file on Firebase and get Download Link
            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        progressDialog.dismiss();
                        firebaseUrl = task.getResult().toString();
                    }
                }
            });
        }
    }


    private void saveData() {
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean dataFilledIn = checkEditText();
                if(dataFilledIn) {
                    setupData();
                    hideEditText();
                    updateInformation();
                }
            }
        });
    }

    private boolean checkEditText() {
        if(TextUtils.isEmpty(nameEditText.getText().toString())){
            nameEditText.setError("Must fill in Name");
            return false;
        }
        if(TextUtils.isEmpty(pledgeAmountEditText.getText().toString())){
            pledgeAmountEditText.setError("Must fill in an Amount");
            return false;
        }
        if(Integer.parseInt(pledgeAmountEditText.getText().toString()) > 2000){
            pledgeAmountEditText.setError("Maximum you can pledge is 2000 kg of CO2e");
            return false;
        }

        return true;
    }

    private void setupData() {

        String name = nameEditText.getText().toString();
        String city = cityText;
        String pledgeAmount = pledgeAmountEditText.getText().toString();
        this.pledgeAmount = Integer.parseInt(pledgeAmount);
        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if(firebaseUrl == null){
            Toast.makeText(getApplicationContext(), "error null", Toast.LENGTH_SHORT).show();
        }
        String profile_pic = firebaseUrl;

        userPledge = new Pledge(name, pledgeAmount, profile_pic, city, userID);

        FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.user_node))
                .child(userID)
                .setValue(userPledge).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Pledge Updated", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar(){
        mDrawerLayout = findViewById(R.id.drawer_layout_edit_pledge);

        Toolbar toolbar = findViewById(R.id.toolbar_edit_pledge);
        toolbar.setTitle(R.string.edit_pledge_page);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.navigation_view_edit_pledge);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case R.id.nav_home:

                        intent = new Intent (getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_pledge:
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                }
                return true;
            }
        });
    }

    private void setUpShareButton(){

        final ShareLinkContent.Builder builder = new ShareLinkContent.Builder();

        // replace to app link when it is released.
        final String url = "www.google.ca";

        shareButton.setEnabled(true);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!inSharing) {
                    inSharing = true;
                    String dialogMessage = "Do you want to share your saving amount as well ?";
                    confirmSharingPledgeAmount(builder, url, dialogMessage);
                }
            }
        });
    }

    private void confirmSharingPledgeAmount(final ShareLinkContent.Builder shareBuilder, final String url, String dialogMessage){
        // sharing messages
        final String msgWithSaving, msgWithoutSaving;
        msgWithoutSaving = "Join me in GreenFoodChallenge!";
        msgWithSaving = String.format("Join me in GreenFoodChallenge, I save %d tons of CO2e!",
                pledgeAmount);

        // generating AlertDialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HelpPopUpTheme);
        builder.setMessage(dialogMessage);

        // set up buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                shareBuilder.setQuote(msgWithSaving);
                shareBuilder.setContentUrl(Uri.parse(url));
                shareButton.setShareContent(shareBuilder.build());
                shareButton.callOnClick();

                // reset the shareButton
                inSharing = false;
                shareButton.setShareContent(null);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                shareBuilder.setQuote(msgWithoutSaving);
                shareBuilder.setContentUrl(Uri.parse(url));
                shareButton.setShareContent(shareBuilder.build());
                shareButton.callOnClick();

                // reset the shareButton
                inSharing = false;
                shareButton.setShareContent(null);
            }
        });

        // creating alert dialog
        AlertDialog dialog = builder.create();


        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public void showPopupOptions(View v){
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_pledge_list_options, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                String item = menuItem.toString();
                if(item == getResources().getString(R.string.edit_pledge_button)){
                    showEditText();
                } else {
                    confirmDelete("You are deleting your profile.");
                }

                return true;
            }
        });
        popup.show();
    }
}
