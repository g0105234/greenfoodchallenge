package ca.sfu.five7.greenfoodchallenge;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ca.sfu.five7.baseclasses.Co2eCalculator;
import ca.sfu.five7.baseclasses.Food;

public class CustomPlanActivity extends AppCompatActivity {

    private static final String EXTRA_RESULT = "ca.sfu.five7.greenfoodchallenge.CustomPlan - the result";

    private final int NUMBER_OF_SLIDER_INCREMENTS = 7;
    private int[] servingArray = new int[7];
    private List<Float> gramsPerServing;
    private List<Float> convertedGrams = new ArrayList<>();
    private Co2eCalculator calculator = new Co2eCalculator();
    private float userResult;
    private float planResult;
    private int saving;
    SeekBar servingSeekBar;
    DrawerLayout mDrawerLayout;
    private String intentKey = "PledgeAmount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_plan);

        extractDataFromIntent();
        initializeArrays();
        initializeCalculator();
        setupAllSeekBars();
        setupPledgeButton();
        setUpNavBar();

    }

    @Override
    protected void onResume(){
        super.onResume();
        Button pledgeButton = findViewById(R.id.custom_pledge_button);
        pledgeButton.setEnabled(true);
    }

    private void extractDataFromIntent(){
        Intent intent = getIntent();
        userResult = intent.getFloatExtra(EXTRA_RESULT, 1500);
    }

    private void initializeArrays(){
        gramsPerServing = new ArrayList<>(Arrays.asList(0.075f,0.075f,0.075f,0.075f,0.090f,0.160f,0.100f));
        for (int i = 0; i < 7; i++){
            servingArray[i] = 0;
            convertedGrams.add(0f);
        }
    }

    private void setupAllSeekBars(){
        setupSeekBar(getResources().getString(R.string.food_1_name), R.id.custom_plan_food_1_seek_bar, R.id.custom_plan_food_1_serving_text_view, 0);
        setupSeekBar(getResources().getString(R.string.food_2_name), R.id.custom_plan_food_2_seek_bar, R.id.custom_plan_food_2_serving_text_view, 1);
        setupSeekBar(getResources().getString(R.string.food_3_name), R.id.custom_plan_food_3_seek_bar, R.id.custom_plan_food_3_serving_text_view, 2);
        setupSeekBar(getResources().getString(R.string.food_4_name), R.id.custom_plan_food_4_seek_bar, R.id.custom_plan_food_4_serving_text_view, 3);
        setupSeekBar(getResources().getString(R.string.food_5_name), R.id.custom_plan_food_5_seek_bar, R.id.custom_plan_food_5_serving_text_view, 4);
        setupSeekBar(getResources().getString(R.string.food_6_name), R.id.custom_plan_food_6_seek_bar, R.id.custom_plan_food_6_serving_text_view, 5);
        setupSeekBar(getResources().getString(R.string.food_7_name), R.id.custom_plan_food_7_seek_bar, R.id.custom_plan_food_7_serving_text_view, 6);
    }

    private void fixTableLabels() {
        checkPlural(R.id.custom_plan_food_1_serving_text_view, R.id.custom_plan_food_1_unit_text_view);
        checkPlural(R.id.custom_plan_food_2_serving_text_view, R.id.custom_plan_food_2_unit_text_view);
        checkPlural(R.id.custom_plan_food_3_serving_text_view, R.id.custom_plan_food_3_unit_text_view);
        checkPlural(R.id.custom_plan_food_4_serving_text_view, R.id.custom_plan_food_4_unit_text_view);
        checkPlural(R.id.custom_plan_food_5_serving_text_view, R.id.custom_plan_food_5_unit_text_view);
        checkPlural(R.id.custom_plan_food_6_serving_text_view, R.id.custom_plan_food_6_unit_text_view);
        checkPlural(R.id.custom_plan_food_7_serving_text_view, R.id.custom_plan_food_7_unit_text_view);
    }

    private void checkPlural(int valueId, int textId) {
        TextView foodServing = findViewById(valueId);
        TextView foodUnit = findViewById(textId);

        if (foodServing.getText().equals("1")) {
            foodUnit.setText(R.string.plan_table_serving_label);
        }else {
            foodUnit.setText(R.string.plan_table_serving_label_plural);
        }
    }

    private void setupSeekBar(final String foodName, int seekBarID, int TextViewID, final int index) {
        final TextView servingTextView = findViewById(TextViewID);
        servingTextView.setText(String.valueOf(0));
        fixTableLabels();
        servingSeekBar = findViewById(seekBarID);
        servingSeekBar.setMax(NUMBER_OF_SLIDER_INCREMENTS);

        servingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int servings, boolean b) {
                servingTextView.setText(String.valueOf(servings));
                servingArray[index] = servings;
                calculateSaving(foodName, index);
                fixTableLabels();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void calculateSaving(String foodName, int index){
        float amount = gramsPerServing.get(index) * servingArray[index];
        convertedGrams.set(index, amount);
        calculator.setAmount(foodName, convertedGrams.get(index));
        planResult = calculator.calculateTotal();
        displaySavings();
    }

    private void initializeCalculator(){
        int index = 0;

        addFoodToCalculator(R.string.food_1_name, R.string.food_1_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_2_name, R.string.food_2_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_3_name, R.string.food_3_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_4_name, R.string.food_4_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_5_name, R.string.food_5_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_6_name, R.string.food_6_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_7_name, R.string.food_7_co2e, index);
    }

    private void addFoodToCalculator(int nameId, int co2eId, int index){
        String foodName = getResources().getString(nameId);
        Float co2ePerGram = Float.parseFloat(getResources().getString(co2eId));
        Food food = new Food(foodName, co2ePerGram);
        calculator.addFood(food, convertedGrams.get(index));
    }

    private void displaySavings(){
        saving = (int) userResult - (int) planResult;
        TextView savingMessageTextView = findViewById(R.id.custom_plan_savings_message_text_view);
        TextView savingTextView = findViewById(R.id.custom_plan_savings_text_view);
        String message;
        if (saving > 0) {
            message = " " + saving + " " + getResources().getString(R.string.plan_units_kg_co2e) + " of CO2e!";
            savingMessageTextView.setVisibility(TextView.VISIBLE);
            savingTextView.setVisibility(TextView.VISIBLE);
            savingTextView.setText(message);
        } else {
            message = getResources().getString(R.string.ineffective_plan_message);
            savingMessageTextView.setVisibility(TextView.GONE);
            savingTextView.setText(message);
        }
        displayVancouverSavings(saving);
        displayEquivalences(saving);
    }

    private void displayVancouverSavings(int saving){
        TextView vancouverSavingMessageTextView = findViewById(R.id.custom_plan_population_savings_message_text_view);
        TextView vancouverSavingTextView = findViewById(R.id.custom_plan_population_savings_text_view);

        if(saving > 0) {
            int vancouverSavings = (int) (2463000 * 0.90 * saving);
            vancouverSavings = vancouverSavings / 1000;
            String message = " " + vancouverSavings + " " + getResources().getString(R.string.plan_units_tonnes_co2e) + " of CO2e!";
            vancouverSavingMessageTextView.setVisibility(TextView.VISIBLE);
            vancouverSavingTextView.setVisibility(TextView.VISIBLE);
            vancouverSavingTextView.setText(message);
        } else {
            vancouverSavingMessageTextView.setVisibility(TextView.GONE);
            vancouverSavingTextView.setVisibility(TextView.GONE);
        }
    }

    private void displayEquivalences(int saving){
        TextView equivalenceTextView = findViewById(R.id.custom_plan_equivalences);
        TextView drivingEquivalenceMessage = findViewById(R.id.custom_plan_equivalences_driving_message);
        TextView drivingEquivalence = findViewById(R.id.custom_plan_equivalences_driving);
        TextView lightEquivalenceMessage = findViewById(R.id.custom_plan_equivalences_lights_message);
        TextView lightEquivalence = findViewById(R.id.custom_plan_equivalences_lights);
        TextView treesEquivalenceMessage = findViewById(R.id.custom_plan_equivalences_trees_message);
        TextView treesEquivalence = findViewById(R.id.custom_plan_equivalences_trees);

        if(saving > 0) {
            String equivalenceMessage = "Saving " + saving + " " + getResources().getString(R.string.plan_units_kg_co2e) + " is equivalent to:";
            equivalenceTextView.setVisibility(TextView.VISIBLE);
            equivalenceTextView.setText(equivalenceMessage);

            //get driving equivalence
            int drivingSaving = saving * 4;
            String drivingMessage = " " + drivingSaving + " " + getResources().getString(R.string.driving_equivalence_2);
            drivingEquivalenceMessage.setVisibility(TextView.VISIBLE);
            drivingEquivalence.setVisibility(TextView.VISIBLE);
            drivingEquivalence.setText(drivingMessage);

            //get switching to LED's
            int lightSaving = (int) (saving * 0.333);
            String lightMessage = " " + lightSaving + " " + getResources().getString(R.string.light_equivalence_2);
            lightEquivalenceMessage.setVisibility(TextView.VISIBLE);
            lightEquivalence.setVisibility(TextView.VISIBLE);
            lightEquivalence.setText(lightMessage);

            //get planting trees
            int treesSaving = (int) (saving / 21.77);
            String treesMessage = " " + treesSaving + " " + getResources().getString(R.string.trees_equivalence_2);
            treesEquivalenceMessage.setVisibility(TextView.VISIBLE);
            treesEquivalence.setVisibility(TextView.VISIBLE);
            treesEquivalence.setText(treesMessage);
        } else {
            equivalenceTextView.setVisibility(TextView.GONE);
            drivingEquivalenceMessage.setVisibility(TextView.GONE);
            drivingEquivalence.setVisibility(TextView.GONE);
            lightEquivalenceMessage.setVisibility(TextView.GONE);
            lightEquivalence.setVisibility(TextView.GONE);
            treesEquivalenceMessage.setVisibility(TextView.GONE);
            treesEquivalence.setVisibility(TextView.GONE);
        }
    }

    private void setupPledgeButton(){
        final Button pledgeButton = findViewById(R.id.custom_pledge_button);
        pledgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pledgeButton.setEnabled(false);

                //start Pledge activity
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                        .child(getString(R.string.user_node))
                        .child(user.getUid());

                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (saving < 0)
                            saving = 0;
                        if(!dataSnapshot.exists()) {
                            Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                            intent.putExtra(intentKey,saving);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                            intent.putExtra(intentKey,saving);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    public static Intent makeIntent(Context context, float result){
        Intent intent = new Intent(context, CustomPlanActivity.class);
        intent.putExtra(EXTRA_RESULT, result);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar() {
        mDrawerLayout = findViewById(R.id.custom_plan_drawer_layout);

        Toolbar toolbar = findViewById(R.id.custom_plan_toolbar);
        toolbar.setTitle(getString(R.string.plans_page));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.navigation_view_custom_plan);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
