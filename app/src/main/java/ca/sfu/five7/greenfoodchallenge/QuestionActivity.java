package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/*
    Question activity
    serving seek bar: used to receive # of servings of the person
    use servingSeekBar.getProgress() to get the int
    serving text view: shows how many servings currently are used
 */

public class QuestionActivity extends AppCompatActivity {
    private int questionNumber;
    private final String servingKey = "ARRAY_DATA";
    private final String inputKey = "ARRAY_INPUTS";
    private float[] answerArray = new float[7];
    private int[] ratioRecord = new int[7];     // record the ratio button id according to user's input
    private int[] servingRecord = new int[7];    // record number of serving inputted by user

    private TextView questionTextView;
    private ImageView centerImage;
    DrawerLayout mDrawerLayout;

    private RadioGroup radioGroup;
    private SeekBar servingSeekBar;
    private TextView servingTextView;
    private ProgressBar questionProgressBar;
    private String question1Text;
    private String question2Text;
    private String question3Text;
    private String question4Text;
    private String question5Text;
    private String question6Text;
    private String question7Text;
    private TextView frequencyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_page);

        initialize();
        setupSeekbar();
        setupHelpButton();
        setupNextButton();
        setUpNavBar();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Button nextButton = findViewById(R.id.questions_next_button);
        nextButton.setEnabled(true);
    }

    private void initialize(){
        initialText();
        initializeVariables();

        questionNumber = 1; //start on question 1
        changeQuestion();
    }

    private void initialText(){
        String genericQuestionText = getResources().getString(R.string.question_label) + " ";
        question1Text = genericQuestionText + getResources().getString(R.string.food_1_name) + "?";
        question2Text = genericQuestionText + getResources().getString(R.string.food_2_name) + "?";
        question3Text = genericQuestionText + getResources().getString(R.string.food_3_name) + "?";
        question4Text = genericQuestionText + getResources().getString(R.string.food_4_name) + "?";
        question5Text = genericQuestionText + getResources().getString(R.string.food_5_name) + "?";
        question6Text = genericQuestionText + getResources().getString(R.string.food_6_name) + "?";
        question7Text = genericQuestionText + getResources().getString(R.string.food_7_name) + "?";
    }

    private void initializeVariables(){
        // initialize components
        radioGroup = findViewById(R.id.questions_portion_radio_group);
        servingTextView = findViewById(R.id.questions_answer_text_view);
        frequencyText = findViewById(R.id.questions_frequency_text_view);
        questionTextView = findViewById(R.id.questions_question_text_view);
        centerImage = findViewById(R.id.questions_center_image);
        questionProgressBar = findViewById(R.id.questions_progress_bar);
        servingSeekBar = findViewById(R.id.questions_answer_seek_bar);
        questionProgressBar.setMax(7);
        int NUMBER_OF_SLIDER_INCREMENTS = 7;
        servingSeekBar.setMax(NUMBER_OF_SLIDER_INCREMENTS);
        // initialize arrays
        for(int i = 0; i < 7; i++){
            servingRecord[i] = 0;
            ratioRecord[i] = R.id.questions_portion_2_radio_button;
        }
    }

    private void setupSeekbar() {
        servingTextView.setText(String.valueOf(0));
        frequencyText.setText(R.string.none);

        servingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int servings, boolean b) {
                servingTextView.setText(String.valueOf(servings));
                switch(servings){
                    case 0:
                        frequencyText.setText(R.string.none);
                        break;
                    case 1:
                        frequencyText.setText(R.string.sometimes);
                        break;
                    case 2:
                        frequencyText.setText(R.string.sometimes);
                        break;
                    case 3:
                        frequencyText.setText(R.string.average);
                        break;
                    case 4:
                        frequencyText.setText(R.string.average);
                        break;
                    case 5:
                        frequencyText.setText(R.string.above_average);
                        break;
                    case 6:
                        frequencyText.setText(R.string.above_average);
                        break;
                    case 7:
                        frequencyText.setText(R.string.all);
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    private void setupHelpButton() {
        Button helpButton = findViewById(R.id.questions_help_button);
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // input texts
                String popUpText = getResources().getString(R.string.questions_help_message);
                ShowPopup(popUpText);
            }
        });
    }

    public void ShowPopup(String message) {
        // generating AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HelpPopUpTheme);
        builder.setMessage(message);
        AlertDialog dialog = builder.create();

        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }


    private void setupNextButton() {
        final Button nextButton = findViewById(R.id.questions_next_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(questionNumber < 7) { //go to next question
                    storeUserInput();
                    questionNumber++;
                    changeQuestion();
                }
                else { //go to review page
                    storeUserInput();
                    nextButton.setEnabled(false);
                    Intent ReviewPage = new Intent(getApplicationContext(), ReviewActivity.class);
                    ReviewPage.putExtra(servingKey, answerArray);
                    ReviewPage.putExtra(inputKey, servingRecord);
                    startActivity(ReviewPage);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    public void goBack(){
        if (questionNumber > 1) { //goes to previous question
            storeUserInput();
            questionNumber--;
            changeQuestion();

        } else if (questionNumber == 1) { //if on question 1, go back to start page
            finish();
        }
    }

    //Changes the question text
    private void changeQuestion() {
        final Button nextButton = findViewById(R.id.questions_next_button);
        servingSeekBar.setProgress(servingRecord[questionNumber-1]);      // set seekbar according to user's input
        radioGroup.check(ratioRecord[questionNumber-1]);      // set radioGroup according to user's input
        questionProgressBar.setProgress(questionNumber);    // set progress bar

        // set question text and image
        switch(questionNumber) {
            case 1:
                questionTextView.setText(question1Text);
                centerImage.setImageResource(R.drawable.cow_image);
                break;
            case 2:
                questionTextView.setText(question2Text);
                centerImage.setImageResource(R.drawable.pig_image);
                break;
            case 3:
                questionTextView.setText(question3Text);
                centerImage.setImageResource(R.drawable.chicken_image);
                break;
            case 4:
                questionTextView.setText(question4Text);
                centerImage.setImageResource(R.drawable.fish_image);
                break;
            case 5:
                questionTextView.setText(question5Text);
                centerImage.setImageResource(R.drawable.egg_image);
                break;
            case 6:
                questionTextView.setText(question6Text);
                centerImage.setImageResource(R.drawable.bean_image);
                nextButton.setText(R.string.next_button);
                break;
            case 7:
                questionTextView.setText(question7Text);
                centerImage.setImageResource(R.drawable.vegetables_image);
                nextButton.setText(R.string.next_button_alt);
                break;
            default:
                break;
        }
    }

    private void storeUserInput(){
        // record input
        ratioRecord[questionNumber-1] = radioGroup.getCheckedRadioButtonId();
        servingRecord[questionNumber-1] = servingSeekBar.getProgress();
        // store answer
        answerArray[questionNumber-1] = servingRecord[questionNumber-1] * getPortionFromRadioGroup();
    }

    private float getPortionFromRadioGroup(){
        switch(radioGroup.getCheckedRadioButtonId()){
            case R.id.questions_portion_1_radio_button:
                return 0.75f;
            case R.id.questions_portion_2_radio_button:
                return 1f;
            case R.id.questions_portion_3_radio_button:
                return 1.25f;
        }
        return 0f;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar() {
        mDrawerLayout = findViewById(R.id.questions_drawer_layout);

        Toolbar toolbar = findViewById(R.id.questions_toolbar);
        toolbar.setTitle(getString(R.string.questions_page));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.questions_navigation_view);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
