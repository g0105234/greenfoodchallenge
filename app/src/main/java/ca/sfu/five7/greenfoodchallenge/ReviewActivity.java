package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ca.sfu.five7.baseclasses.Co2eCalculator;
import ca.sfu.five7.baseclasses.Food;

public class ReviewActivity extends AppCompatActivity {
    DrawerLayout mDrawerLayout;
    float[] inputData;
    int[] inputDataRaw;
    List<Float> convertedInputData;
    Co2eCalculator calculator;
    final String servingKey = "ARRAY_DATA";   // servingKey for getting data from previous activity
    final String inputKey = "ARRAY_INPUTS";   // inputKey for getting raw serving inputs from previous activity
    final String totalKey = "TOTAL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_page);

        convertRawInput();
        initialize();
        listenCalculateButton();
        setUpNavBar();
        setupHelpButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Button calculateButton = findViewById(R.id.review_calculate_button);
        calculateButton.setEnabled(true);
    }

    private void convertRawInput(){
        try {
            List<Float> ratio = new ArrayList<>(Arrays.asList(0.075f,0.075f,0.075f,0.075f,0.090f,0.160f,0.100f));
            inputData = getIntent().getFloatArrayExtra(servingKey);
            convertedInputData = new ArrayList<>();
            float amount;
            for (int i = 0; i < inputData.length; i++) {
                //formula here
                amount = inputData[i] * ratio.get(i);
                convertedInputData.add(amount);
            }
        }catch(NullPointerException e){
            Log.e("NullPointer", "Null-pointer detected while passing data from Question Activity to Review Activity");
        }
    }

    /*
        Initialization
     */
    private void initialize(){
        inputDataRaw = getIntent().getIntArrayExtra(inputKey);

        int index = 0;
        addTextView(R.id.review_food_1_name_text_view, R.id.review_food_1_days_text_view,
                R.id.review_food_1_amount_text_view, R.string.food_1_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_1_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_2_name_text_view, R.id.review_food_2_days_text_view,
                R.id.review_food_2_amount_text_view, R.string.food_2_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_2_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_3_name_text_view, R.id.review_food_3_days_text_view,
                R.id.review_food_3_amount_text_view, R.string.food_3_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_3_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_4_name_text_view, R.id.review_food_4_days_text_view,
                R.id.review_food_4_amount_text_view, R.string.food_4_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_4_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_5_name_text_view, R.id.review_food_5_days_text_view,
                R.id.review_food_5_amount_text_view, R.string.food_5_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_5_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_6_name_text_view, R.id.review_food_6_days_text_view,
                R.id.review_food_6_amount_text_view, R.string.food_6_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_6_progress_bar, inputDataRaw[index]);

        index += 1;
        addTextView(R.id.review_food_7_name_text_view, R.id.review_food_7_days_text_view,
                R.id.review_food_7_amount_text_view, R.string.food_7_label, inputDataRaw[index], convertedInputData.get(index));
        addBar(R.id.review_food_7_progress_bar, inputDataRaw[index]);

        initializeCalculator();

    }

    private void addBar(int progressBarId, int amount) {
        ProgressBar barFood;
        barFood = findViewById(progressBarId);
        barFood.setMax(7);
        barFood.setProgress(amount);
    }


    private void addTextView(int textViewNameId, int textViewDaysId, int textViewAmountId, int foodNameId, int days, float amount){
        TextView textViewName = findViewById(textViewNameId);
        TextView textViewDays = findViewById(textViewDaysId);
        TextView textViewAmount = findViewById(textViewAmountId);

        String foodName = getResources().getString(foodNameId);
        String foodDays;
        if (days == 1 ){
            foodDays = days + " Day ";
        }else {
            foodDays = days + " Days";
        }

        String foodAmount = String.format("%.2f kg", amount);

        textViewAmount.setText(foodAmount);
        textViewDays.setText(foodDays);
        textViewName.setText(foodName);
    }

    private void initializeCalculator(){
        calculator = new Co2eCalculator();
        int index = 0;

        addFoodToCalculator(R.string.food_1_name, R.string.food_1_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_2_name, R.string.food_2_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_3_name, R.string.food_3_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_4_name, R.string.food_4_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_5_name, R.string.food_5_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_6_name, R.string.food_6_co2e, index);
        index += 1;
        addFoodToCalculator(R.string.food_7_name, R.string.food_7_co2e, index);
    }

    private void addFoodToCalculator(int nameId, int co2eId, int index){
        String foodName = getResources().getString(nameId);
        Float co2ePerGram = Float.parseFloat(getResources().getString(co2eId));
        Food food = new Food(foodName, co2ePerGram);
        calculator.addFood(food, convertedInputData.get(index));
    }

    private void listenCalculateButton(){

        final Button calculateButton = findViewById(R.id.review_calculate_button);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateButton.setEnabled(false);
                float totalCo2e = calculator.calculateTotal();
                // go to result page
                Intent ResultPageIntent = new Intent(getApplicationContext(), ResultsPageActivity.class);
                ResultPageIntent.putExtra(totalKey, totalCo2e);
                startActivity(ResultPageIntent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupHelpButton() {
        Button helpButton = findViewById(R.id.review_help_button);
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // input texts
                String popUpText = getResources().getString(R.string.review_help_message);
                ShowPopup(popUpText);
            }
        });
    }

    public void ShowPopup(String message) {
        // generating AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HelpPopUpTheme);
        builder.setMessage(message);
        AlertDialog dialog = builder.create();

        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    private void setUpNavBar() {
        mDrawerLayout = findViewById(R.id.drawer_layout_review);

        Toolbar toolbar = findViewById(R.id.review_toolbar);
        toolbar.setTitle(getString(R.string.review_page));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.review_navigation_view);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }

}
