package ca.sfu.five7.greenfoodchallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/*
    TODO
        finish help button when help page done
 */

public class ResultsPageActivity extends AppCompatActivity {

    final String totalKey = "TOTAL";
    private Float total;
    DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_page);

        initializeTextView();
        listenButtons();
        setUpNavBar();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Button planButton1 = findViewById(R.id.results_meat_eater_plan_button);
        planButton1.setEnabled(true);
        Button planButton2 = findViewById(R.id.results_low_meat_plan_button);
        planButton2.setEnabled(true);
        Button planButton3 = findViewById(R.id.results_plant_based_plan_button);
        planButton3.setEnabled(true);
        Button customPlanButton = findViewById(R.id.results_custom_plan_button);
        customPlanButton.setEnabled(true);
        Button homeButton = findViewById(R.id.results_home_button);
        homeButton.setEnabled(true);
    }

    void initializeTextView(){
        total = getIntent().getFloatExtra(totalKey,0);
        TextView resultTextView = findViewById(R.id.results_text_view);
        String output = String.format("%.2f Kgs CO2e per Year",total);
        resultTextView.setText(output);

    }

    void listenButtons(){
        listenPlanButton(R.id.results_meat_eater_plan_button,1);
        listenPlanButton(R.id.results_low_meat_plan_button,2);
        listenPlanButton(R.id.results_plant_based_plan_button, 3);
        listenCustomPlanButton();
        listenHomeButton();
        //listenHelpButton();
    }

    void listenPlanButton(int buttonId, final int passingValue){
        final Button planButton = findViewById(buttonId);
        planButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                planButton.setEnabled(false);

                Intent intent = PlanPageActivity.makeIntent(getApplicationContext(), total, passingValue);
                startActivity(intent);
            }
        });
    }

    void listenCustomPlanButton(){
        final Button customPlanButton = findViewById(R.id.results_custom_plan_button);
        customPlanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customPlanButton.setEnabled(false);

                Intent intent = CustomPlanActivity.makeIntent(getApplicationContext(), total);
                startActivity(intent);
            }
        });
    }

    void listenHomeButton(){
        final Button homeButton = findViewById(R.id.results_home_button);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeButton.setEnabled(false);

                Intent intent = new Intent(getApplicationContext(), StartPageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar() {
        mDrawerLayout = findViewById(R.id.results_drawer_layout);

        Toolbar toolbar = findViewById(R.id.results_toolbar);
        toolbar.setTitle(getString(R.string.results_page));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.results_navigation_view);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                Intent intent;
                switch (id){
                    case  R.id.nav_home:

                        intent = new Intent(getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;


                    case R.id.nav_community:

                        intent = new Intent(getApplicationContext(), CommunityPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                }
                return true;
            }
        });
    }
}
