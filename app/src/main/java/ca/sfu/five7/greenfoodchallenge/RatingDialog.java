package ca.sfu.five7.greenfoodchallenge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ca.sfu.five7.databaseclass.Meal;

public class RatingDialog extends DialogFragment {

    public final static String owner_id_key = "owner_id_key";
    public final static String meal_id_key = "meal_id_key";
    public final static String user_id_key = "user_id_key";

    String user_id;
    String owner_id;
    String meal_id;
    Meal meal;
    View view;
    ImageView star1;
    ImageView star2;
    ImageView star3;
    ImageView star4;
    ImageView star5;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.dialog_rating, null);
        builder.setView(view);

        initialize();
        getDataFromDataBase();
        listenRateButton();

        return builder.create();
    }

    public static RatingDialog newInstance(String owner_id, String meal_id, String user_id){
        RatingDialog ratingDialog = new RatingDialog();
        Bundle args = new Bundle();
        args.putString(owner_id_key, owner_id);
        args.putString(meal_id_key, meal_id);
        args.putString(user_id_key, user_id);
        ratingDialog.setArguments(args);
        return ratingDialog;
    }

    private void initialize(){
        Bundle args = getArguments();
        user_id = args.getString(RatingDialog.user_id_key);
        meal_id = args.getString(RatingDialog.meal_id_key);
        owner_id = args.getString(RatingDialog.owner_id_key);
        star1 = view.findViewById(R.id.star_1);
        star2 = view.findViewById(R.id.star_2);
        star3 = view.findViewById(R.id.star_3);
        star4 = view.findViewById(R.id.star_4);
        star5 = view.findViewById(R.id.star_5);
    }

    private void listenRateButton(){
        Button rateButton = view.findViewById(R.id.confirm_rate_button);
        final SeekBar ratingSeekBar = view.findViewById(R.id.rating_bar);
        ratingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                switch(ratingSeekBar.getProgress()){
                    case 0:
                        star1.setImageResource(R.drawable.empty_star);
                        star2.setImageResource(R.drawable.empty_star);
                        star3.setImageResource(R.drawable.empty_star);
                        star4.setImageResource(R.drawable.empty_star);
                        star5.setImageResource(R.drawable.empty_star);
                        break;
                    case 1:
                        star1.setImageResource(R.drawable.full_star);
                        star2.setImageResource(R.drawable.empty_star);
                        star3.setImageResource(R.drawable.empty_star);
                        star4.setImageResource(R.drawable.empty_star);
                        star5.setImageResource(R.drawable.empty_star);
                        break;
                    case 2:
                        star1.setImageResource(R.drawable.full_star);
                        star2.setImageResource(R.drawable.full_star);
                        star3.setImageResource(R.drawable.empty_star);
                        star4.setImageResource(R.drawable.empty_star);
                        star5.setImageResource(R.drawable.empty_star);
                        break;
                    case 3:
                        star1.setImageResource(R.drawable.full_star);
                        star2.setImageResource(R.drawable.full_star);
                        star3.setImageResource(R.drawable.full_star);
                        star4.setImageResource(R.drawable.empty_star);
                        star5.setImageResource(R.drawable.empty_star);
                        break;
                    case 4:
                        star1.setImageResource(R.drawable.full_star);
                        star2.setImageResource(R.drawable.full_star);
                        star3.setImageResource(R.drawable.full_star);
                        star4.setImageResource(R.drawable.full_star);
                        star5.setImageResource(R.drawable.empty_star);
                        break;
                    case 5:
                        star1.setImageResource(R.drawable.full_star);
                        star2.setImageResource(R.drawable.full_star);
                        star3.setImageResource(R.drawable.full_star);
                        star4.setImageResource(R.drawable.full_star);
                        star5.setImageResource(R.drawable.full_star);
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (meal == null) {
                    Toast.makeText(getContext(),"null meal", Toast.LENGTH_SHORT).show();
                    return;
                }
                meal.addRating(user_id, ratingSeekBar.getProgress());
                FirebaseDatabase.getInstance().getReference()
                        .child(getString(R.string.meal_node))
                        .child(owner_id)
                        .child(meal_id)
                        .setValue(meal);
                dismissDialog();
            }
        });
    }

    public void dismissDialog() {
        getActivity().startActivityForResult(getActivity().getIntent(), 10);
        dismiss();
    }

    private void getDataFromDataBase(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(getString(R.string.meal_node)).child(owner_id).child(meal_id);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                meal = dataSnapshot.getValue(Meal.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
