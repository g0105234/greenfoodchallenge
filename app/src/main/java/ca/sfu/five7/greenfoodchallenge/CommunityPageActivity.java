package ca.sfu.five7.greenfoodchallenge;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ca.sfu.five7.databaseclass.Pledge;
import ca.sfu.five7.adapters.PledgeList;

public class CommunityPageActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    ListView mListView;
    private TextView totalAmountPledgedTextView;
    private TextView totalAmountOfUsersTextView;
    private FloatingActionButton filterButton;

    List<Pledge> mPledgeList;
    float totalAmountPledged;
    int totalAmountOfUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_page);

        mListView = findViewById(R.id.community_list_view);
        totalAmountPledgedTextView = findViewById(R.id.total_amount_pledged);
        totalAmountOfUsersTextView = findViewById(R.id.total_amount_of_users);
        filterButton = findViewById(R.id.community_filter_button);

        mPledgeList = new ArrayList<>();

        setUpNavBar();
        setupFilterButton();

    }

    @Override
    protected void onStart() {
        super.onStart();
        getDataFromDatabase(getString(R.string.filter_all_cities));
    }

    @Override
    protected void onResume(){
        super.onResume();
        getDataFromDatabase(getString(R.string.filter_all_cities));
    }

    private void getDataFromDatabase(final String city) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(getString(R.string.user_node));

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mPledgeList.clear();
                totalAmountPledged = 0;
                totalAmountOfUsers = 0;

                for(DataSnapshot pledgeSnapshot : dataSnapshot.getChildren()){
                    Pledge pledge = pledgeSnapshot.getValue(Pledge.class);

                    totalAmountPledged += Integer.parseInt(pledge.getPledge_amount());
                    totalAmountOfUsers++;

                    if (city.equals(getString(R.string.filter_all_cities))){
                        mPledgeList.add(pledge);
                    } else {
                        if(pledge.getCity().equals(city)){
                            mPledgeList.add(pledge);
                        }
                    }
                }

                PledgeList adapter = new PledgeList(CommunityPageActivity.this, mPledgeList);
                mListView.setAdapter(adapter);
                setListViewHeightBasedOnItems(mListView);
                setupText();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setupFilterButton(){
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(getApplicationContext(), filterButton);
                popupMenu.getMenuInflater().inflate(R.menu.community_page_filter_options, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String itemText = item.toString();
                        if(itemText.equals(getString(R.string.filter_by_city_title))){
                            createCityFilterDialog();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void createCityFilterDialog(){
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(R.string.city_filter_dialog_title)
                .setItems(R.array.metro_vancouver_cities_filter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String cityChosen = getResources().getStringArray(R.array.metro_vancouver_cities_filter)[which];
                getDataFromDatabase(cityChosen);
            }
        });

        // creating alert dialog
        android.app.AlertDialog dialog = builder.create();


        // UI setting
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public void setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int maximumLength;
            if (listAdapter.getCount() > 10){
                maximumLength = 10;
            } else {
                maximumLength = listAdapter.getCount();
            }

            // Get height of items
            int totalItemHeight = 0;
            for (int position = 0; position < maximumLength; position++) {
                View item = listAdapter.getView(position, null, listView);
                item.measure(0, 0);
                totalItemHeight += item.getMeasuredHeight();
            }

            // Get height of dividers
            int totalDividersHeight = listView.getDividerHeight() * (maximumLength - 1);

            // Set list height.
            ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
            layoutParams.height = totalItemHeight + totalDividersHeight;
            listView.setLayoutParams(layoutParams);
            listView.requestLayout();
        }
    }

    private void setupText(){
        //Set up strings to display
        float totalAmountInTonnes = totalAmountPledged / 1000;
        String totalInTonnes = String.format("%.2f",totalAmountInTonnes);
        String totalUsers = " " + totalAmountOfUsers;
        String totalPledged = totalInTonnes;

        totalAmountOfUsersTextView.setText(totalUsers);
        totalAmountPledgedTextView.setText(totalPledged);

        //set up average pledge
        float averagePledge = totalAmountPledged/totalAmountOfUsers;
        TextView averagePledgeTextView = findViewById(R.id.average_pledge_text_view);
        String averagePledgeOutput =String.format("%.1f",averagePledge);
        averagePledgeTextView.setText(averagePledgeOutput);

        displayEquivalences((int)totalAmountPledged);
    }

    private void displayEquivalences(int saving){
        TextView equivalenceTextView = findViewById(R.id.community_equivalences_text_view);
        float totalAmountInTonnes = totalAmountPledged / 1000;
        String totalInTonnes = String.format("%.2f",totalAmountInTonnes);
        String equivalenceMessage =totalInTonnes + " " +  getResources().getString(R.string.plan_units_tonnes_co2e) + " is equivalent to:";
        equivalenceTextView.setText(equivalenceMessage);

        //get driving equivalence
        TextView drivingEquivalence = findViewById(R.id.community_equivalences_driving);
        int drivingSaving = saving * 4;
        String drivingMessage = drivingSaving + "";
        drivingEquivalence.setText(drivingMessage);

        //get switching to LED's
        TextView lightEquivalence = findViewById(R.id.community_equivalences_lights);
        int lightSaving = (int) ((saving * 0.333)*50)/1000;
        String lightMessage = lightSaving + "";
        lightEquivalence.setText(lightMessage);

        //get planting trees
        TextView treesEquivalence = findViewById(R.id.community_equivalences_trees);
        int treesSaving = (int) (saving / 21.77);
        String treesMessage = treesSaving + " ";
        treesEquivalence.setText(treesMessage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpNavBar(){
        mDrawerLayout = findViewById(R.id.drawer_layout_community);

        Toolbar toolbar = findViewById(R.id.toolbar_community);
        toolbar.setTitle(R.string.community_page);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.menu_icon_white);


        final NavigationView nav_view = findViewById(R.id.navigation_view_community);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();
                Intent intent;
                switch(id){
                    case R.id.nav_home:

                        intent = new Intent (getApplicationContext(), StartPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_pledge:

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(getString(R.string.user_node))
                                .child(user.getUid());

                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!dataSnapshot.exists()) {
                                    Intent intent = new Intent(getApplicationContext(), AddPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), EditPledgePageActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_community:

                        //update community page
                        getDataFromDatabase(getString(R.string.filter_all_cities));
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_about:

                        intent = new Intent(getApplicationContext(), AboutPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_sign_out:

                        FirebaseAuth.getInstance().signOut();

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.default_web_client_id))
                                .requestProfile()
                                .requestEmail()
                                .build();

                        GoogleSignIn.getClient(getApplicationContext(), gso).signOut();
                        intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // kill all associated activities.
                        startActivity(intent);
                        return true;

                    case R.id.nav_my_meal:
                        intent = new Intent(getApplicationContext(), MyMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                    case R.id.nav_view_meals:
                        intent = new Intent(getApplicationContext(), ViewMealActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;

                }
                return true;
            }
        });
    }

}
