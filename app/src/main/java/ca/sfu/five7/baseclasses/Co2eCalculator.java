package ca.sfu.five7.baseclasses;

import java.util.ArrayList;
import java.util.List;

/*
    Add food and amount(grams) into the Co2eCalculator, and use the calculate total to obtain the total
    amount of co2e

    Uses:
        Class Food
        Food::getName
        Food::getCo2ePerGram
 */
public class Co2eCalculator {

    /*
        The class FoodNAmount is used to store Food and the amount. It can calculate amount of co2e
        consumed by this amount of food
     */
    class FoodNAmount{
        Food food;
        float amount;   // unit: gram
        FoodNAmount(Food _food, float _amount){food = _food; amount = _amount;}

        public void setAmount(float amount) {
            this.amount = amount;
        }

        public String getName(){
            return food.getName();
        }

        /*
            return the amount of Co2e of this amount of food
         */
        public float getCo2e(){
            return food.co2ePerGram*amount;
        }
    }

    List<FoodNAmount> foodList;

    public Co2eCalculator(){
        foodList = new ArrayList<>();
    }

    /*
        add the food and its amount(grams) into the list
     */
    public void addFood(Food food, float amount){
        FoodNAmount foodNAmount = new FoodNAmount(food, amount);
        foodList.add(foodNAmount);
    }

    /*
        set the amount(grams) of food
     */
    public void setAmount(String name, float amount){
        int index = searchFood(name);
        if (index == -1)
            return;
        else
            foodList.get(index).setAmount(amount);
    }

    /*
        remove food from list according to name
     */
    public void removeFood(String name){
        int index = searchFood(name);
        if (index == -1)
            return;
        else
            foodList.remove(index);
    }

    /*
        return the total amount of co2e in the list
     */
    public float calculateTotal(){
        float total = 0;
        int size = foodList.size();
        for (int i = 0; i < size; i++){
            total += foodList.get(i).getCo2e();
        }
        return total*52;    // return one year(52 weeks) consumption of co2e
    }

    /*
        input food name and return the proper index
        return -1 if not found
     */
    private int searchFood(String name) {
        int size = foodList.size();
        if (size == 0)
            return -1;
        for (int i = 0; i < size; i++){
            String foodName = foodList.get(i).getName();
            if (foodName.equals(name)){
                return i;
            }
        }
        return -1;
    }

}
