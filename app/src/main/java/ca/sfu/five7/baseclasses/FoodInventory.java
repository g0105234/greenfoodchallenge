package ca.sfu.five7.baseclasses;

import java.util.ArrayList;
import java.util.List;

/*
    The class FoodInventory store Food in a list and has function to manipulate the list

    Uses:
        Class Food
        Food::getName
 */
public class FoodInventory {
    List<Food> foodList;

    FoodInventory(){
        foodList = new ArrayList<>();
    }

    /*
        add Food onto the list
     */
    public void addFood(String name, float co2ePerGram){
        Food newFood = new Food(name, co2ePerGram);
        foodList.add(newFood);
    }

    /*
        return Food from list according to index
     */
    public Food getFood(int index){
        return foodList.get(index);
    }

    /*
        return Food from list according to name
     */
    public Food getFood(String name){
        int index = searchFood(name);
        if (index == -1)
            return null;
        else
            return foodList.get(index);
    }

    /*
        remove Food from list according to name
     */
    public void removeFood(String name){
        int index = searchFood(name);
        foodList.remove(index);
    }

    /*
        search Food according to name and return the index
     */
    private int searchFood(String name) {
        int size = foodList.size();
        Food currentFood;
        if (size <= 0)
            return -1;
        for (int i = 0; i < size; i++) {
            currentFood = foodList.get(i);
            if (currentFood.getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
}
