package ca.sfu.five7.baseclasses;

/*
    Class Food contains the name and co2ePerGram, and some getter function
 */
public class Food {
    String name;
    float co2ePerGram;

    public Food(String name, float Co2ePerGram){
        this.name = name;
        this.co2ePerGram = Co2ePerGram;
    }

    public String getName(){
        return name;
    }

    public float getCo2ePerGram(){
        return co2ePerGram;
    }
}
