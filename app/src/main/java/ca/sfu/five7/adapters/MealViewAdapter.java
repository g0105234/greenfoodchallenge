package ca.sfu.five7.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;

import java.util.List;

import ca.sfu.five7.databaseclass.Meal;
import ca.sfu.five7.greenfoodchallenge.R;
import ca.sfu.five7.greenfoodchallenge.ViewMealDialog;

public class MealViewAdapter extends RecyclerView.Adapter<MealViewAdapter.ViewHolder>{

    private List<Meal> mMeals;
    private List<String> mOwnerList;
    private Context mContext;
    private int counter;
    private android.support.v4.app.FragmentManager mFragmentManager;

    public MealViewAdapter(List<String> ownerList, List<Meal> meals, Context context, android.support.v4.app.FragmentManager fragmentManager) {
        mMeals = meals;
        mOwnerList = ownerList;
        mContext = context;
        counter = 0;
        mFragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public MealViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.meals_view_recycler_layout, viewGroup, false);
        return new MealViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MealViewAdapter.ViewHolder viewHolder, int position) {
        for (int i = 0; i < 3; i++){
            int index = position*3+i;
            if (index < mMeals.size()){
                ImageButton imageButton;
                if (i == 0){
                    imageButton = viewHolder.button1;
                }else if (i == 1){
                    imageButton = viewHolder.button2;
                }else{
                    imageButton = viewHolder.button3;
                }
                imageButton.setVisibility(View.VISIBLE);
                final Meal meal = mMeals.get(index);
                final String owner_id = mOwnerList.get(index);
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createDialog(meal, owner_id);
                    }
                });
                setSquare(imageButton);
                //set image
                Glide.with(mContext).load(meal.getMeal_pic()).into(imageButton);
            }
        }


    }

    private void setSquare(ImageButton imageButton){
        ViewGroup.LayoutParams params = imageButton.getLayoutParams();
        params.height =  Resources.getSystem().getDisplayMetrics().widthPixels/3;
        params.width =  Resources.getSystem().getDisplayMetrics().widthPixels/3;
        imageButton.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return 1 + mMeals.size()/3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        public ImageButton button1;
        public ImageButton button2;
        public ImageButton button3;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            button1 = itemView.findViewById(R.id.meal_view_img_button1);
            button2 = itemView.findViewById(R.id.meal_view_img_button2);
            button3 = itemView.findViewById(R.id.meal_view_img_button3);
        }
    }

    private void createDialog(Meal meal, String owner__id){
        DialogFragment dialogFragment = new ViewMealDialog().newInstance(owner__id, meal.getMeal_id());
        dialogFragment.show(mFragmentManager, "view");
    }
}
