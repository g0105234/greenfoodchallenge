/*
    This class is used to setup the view for My Meal Activity
*/

package ca.sfu.five7.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;

import ca.sfu.five7.greenfoodchallenge.R;
import ca.sfu.five7.databaseclass.Meal;

public class MyMealAdapter extends RecyclerView.Adapter<MyMealAdapter.ViewHolder> {

    private List<Meal> mMeals;
    private Context mContext;

    public MyMealAdapter(List<Meal> meals, Context context) {
        mMeals = meals;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.my_meal_recycler_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Meal mealItem = mMeals.get(position);

        viewHolder.nameTextView.setText(mealItem.getMeal_name());
        viewHolder.restaurantTextView.setText(mealItem.getRestaurant_name());
        viewHolder.proteinTextView.setText(mealItem.getProtein_type());
        viewHolder.locationTextView.setText(mealItem.getLocation());
        viewHolder.ratingTextView.setText(String.format("%.1f", mealItem.getAverage_rating()));
        viewHolder.favouriteTextView.setText(String.valueOf(mealItem.getFavourite_list().size()));

        if(TextUtils.isEmpty(mealItem.getDescription())){
            viewHolder.descriptionTextView.setVisibility(TextView.GONE);
        } else {
            viewHolder.descriptionTextView.setText(mealItem.getDescription());
        }

        Glide.with(mContext).load(mealItem.getMeal_pic()).into(viewHolder.mealImage);
        viewHolder.mealID = mealItem.getMeal_id();

        //Setup Delete Button
        viewHolder.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.HelpPopUpTheme);
                String message = mContext.getString(R.string.meal_delete_message);
                builder.setMessage(message);

                // set up buttons
                builder.setPositiveButton(mContext.getString(R.string.meal_delete_positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //delete from database
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child(mContext.getString(R.string.meal_node))
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .child(mealItem.getMeal_id());

                        ref.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                            }
                        });

                        //delete from storage if it is not default image
                        if (!mealItem.getMeal_pic().equals(mContext.getResources().getString(R.string.default_meal_image)))
                            FirebaseStorage.getInstance().getReferenceFromUrl(mealItem.getMeal_pic()).delete();
                    }
                });

                builder.setNegativeButton(mContext.getString(R.string.meal_delete_negative_button), null);

                // creating alert dialog
                AlertDialog dialog = builder.create();

                // UI setting
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                dialog.show();
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mMeals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        public TextView favouriteTextView;
        public TextView nameTextView;
        public TextView restaurantTextView;
        public TextView proteinTextView;
        public TextView locationTextView;
        public TextView descriptionTextView;
        public TextView ratingTextView;
        public ImageView mealImage;
        public ImageView deleteImage;
        public String mealID;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.meal_name);
            restaurantTextView = itemView.findViewById(R.id.meal_restaurant);
            proteinTextView = itemView.findViewById(R.id.meal_protein);
            locationTextView = itemView.findViewById(R.id.meal_location);
            descriptionTextView = itemView.findViewById(R.id.meal_description);
            mealImage = itemView.findViewById(R.id.meal_image);
            deleteImage = itemView.findViewById(R.id.meal_delete_image);
            ratingTextView = itemView.findViewById(R.id.ratings_display);
            favouriteTextView = itemView.findViewById(R.id.favourite_display);
        }
    }
}
