/*
    This class is used to populate the list with all the users on the database.
*/

package ca.sfu.five7.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ca.sfu.five7.databaseclass.Pledge;
import ca.sfu.five7.greenfoodchallenge.R;

public class PledgeList extends ArrayAdapter<Pledge> {

    private Activity mContext;
    private List<Pledge> mPledgeList;

    public PledgeList(Activity context, List<Pledge> pledgeList){
        super(context, R.layout.list_community_layout, pledgeList);
        mContext = context;
        mPledgeList = pledgeList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_community_layout, null, true);

        TextView userName = (TextView) listViewItem.findViewById(R.id.community_user_name);
        TextView userCity = (TextView)  listViewItem.findViewById(R.id.community_user_city);
        TextView userPledgeAmount = (TextView)  listViewItem.findViewById(R.id.community_user_pledge_amount);
        ImageView userPic = (ImageView) listViewItem.findViewById(R.id.community_user_pic);

        Pledge pledge = mPledgeList.get(position);

        userName.setText(pledge.getName());
        userCity.setText(pledge.getCity());
        userPledgeAmount.setText(pledge.getPledge_amount());
        Glide.with(mContext).load(pledge.getProfile_pic()).into(userPic);

        return listViewItem;
    }

}
