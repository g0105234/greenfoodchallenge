# EcoSmart (greenfoodchallenge)

EcoSmart is a CO2e diet calculator and meal sharing smartphone app. EcoSmart strives to create a community devoted to reducing CO2e output by making dietary decisions that reduce our carbon footprint. A user will enter their dietary habits and the app will calculate their CO2e output and make suggestions on how to reduce their diet’s CO2e output. Furthermore, the app contains a community section where users can view their community’s carbon footprint reduction, post pictures of meals, and share restaurant meals that have a low carbon footprint. 


### Prerequisites

Require Android api 21 or above


### Installing

Install the app on Android phones.


## Authors

* **Zhi Gang Yao, Evan** - [greenfoodchallenge](https://csil-git1.cs.surrey.sfu.ca/7-Five7/greenfoodchallenge)
* **Oleh Sokolov** - [greenfoodchallenge](https://csil-git1.cs.surrey.sfu.ca/7-Five7/greenfoodchallenge)
* **Lindon Kwok** - [greenfoodchallenge](https://csil-git1.cs.surrey.sfu.ca/7-Five7/greenfoodchallenge)
* **Mantek Mann** - [greenfoodchallenge](https://csil-git1.cs.surrey.sfu.ca/7-Five7/greenfoodchallenge)
* **Riley Bromley** - [greenfoodchallenge](https://csil-git1.cs.surrey.sfu.ca/7-Five7/greenfoodchallenge)

## Screenshots
<img src="AppScreenshots/About_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Help_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Home_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Plan_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Custom_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Question_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Results_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Review_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Community_Screenshot.png" height=500>
<img src="AppScreenshots/Edit_Pledge_Screenshot.png" height=500>
<img src="AppScreenshots/SignIn_Page_Screenshot.png" height=500>
<img src="AppScreenshots/My_Meal_Page_Screenshot.png" height=500>
<img src="AppScreenshots/OnClick_View_Meal_Screenshot.png" height=500>
<img src="AppScreenshots/View_Meals_Page_Screenshot.png" height=500>
<img src="AppScreenshots/Add_Pledge_Screenshot.png" height=500>
